<?php

use Illuminate\Database\Seeder;

class EstadoCivilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\EstadoCivil::query()->insert([
            ['descripcion' => 'Casado/a'],
            ['descripcion' => 'Soltero/a'],
            ['descripcion' => 'Unión libre o unión de hecho'],
            ['descripcion' => 'Separado/a'],
            ['descripcion' => 'Divorciado/a'],
            ['descripcion' => 'Viudo/a'],
        ]);
    }
}

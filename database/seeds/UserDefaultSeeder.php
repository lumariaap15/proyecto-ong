<?php

use Illuminate\Database\Seeder;

class UserDefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class)->create([
            'email' => 'admin@gmail.com'
        ]);

        factory(\App\User::class)->create([
            'email' => 'casos@gmail.com',
            'role'=>'casos'
        ]);

        factory(\App\User::class)->create([
            'email' => 'consulta@gmail.com',
            'role'=>'consulta'
        ]);
    }
}

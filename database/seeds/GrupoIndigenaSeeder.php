<?php

use Illuminate\Database\Seeder;

class GrupoIndigenaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Models\GrupoIndigena::query()->insert([
            ['descripcion' => 'Achagua'],
            ['descripcion' => 'Amorúa'],
            ['descripcion' => 'Andoke'],
            ['descripcion' => 'Arhuaco'],
            ['descripcion' => 'Awa'],
            ['descripcion' => 'Bara'],
            ['descripcion' => 'Barasana'],
            ['descripcion' => 'Barí'],
            ['descripcion' => 'Betoye'],
            ['descripcion' => 'Bora'],
            ['descripcion' => 'Cañamomo'],
            ['descripcion' => 'Carapana'],
            ['descripcion' => 'Cocama'],
            ['descripcion' => 'Chimila'],
            ['descripcion' => 'Chiricoa'],
            ['descripcion' => 'Coconuco'],
            ['descripcion' => 'Coreguaje'],
            ['descripcion' => 'Coyaima-Natagaima'],
            ['descripcion' => 'Desano'],
            ['descripcion' => 'Dujo'],
            ['descripcion' => 'Embera'],
            ['descripcion' => 'Embera Katío'],
            ['descripcion' => 'Embera - Chamí'],
            ['descripcion' => 'Eperara - Siapidara'],
            ['descripcion' => 'Guambiano'],
            ['descripcion' => 'Guanaca'],
            ['descripcion' => 'Guane'],
            ['descripcion' => 'Guayabero'],
        ]);
    }
}

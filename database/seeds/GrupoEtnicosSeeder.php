<?php

use Illuminate\Database\Seeder;

class GrupoEtnicosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\GrupoEtnico::query()->insert([
            ['descripcion' => 'Indígenas'],
            ['descripcion' => 'Mestizos'],
            ['descripcion' => 'Caucásicos'],
            ['descripcion' => 'Afrocolombianos'],
        ]);
    }
}

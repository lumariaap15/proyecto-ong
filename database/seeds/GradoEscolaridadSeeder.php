<?php

use Illuminate\Database\Seeder;

class GradoEscolaridadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\GradoEscolaridad::query()->insert([
            ['descripcion' => 'Preescolar'],
            ['descripcion' => 'Básica'],
            ['descripcion' => 'Media'],
            ['descripcion' => 'superior'],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class TipoDocumentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Models\TipoDocumento::query()->insert([
            ['descripcion'=>'NUMERO DE IDENTIFICACIÓN TRIBUTARIA','sigla'=>'NIT'],
            ['descripcion'=>'CÉDULA DE CIUDADANÍA','sigla'=>'CC'],
            ['descripcion'=>'TARJETA DE IDENTIDAD','sigla'=>'TI'],
            ['descripcion'=>'CÉDULA DE EXTRANJERÍA','sigla'=>'CE'],
            ['descripcion'=>'REGISTRO CIVIL DE NACIMIENTO','sigla'=>'RC'],
            ['descripcion'=>'PASAPORTE','sigla'=>'PS'],
            ['descripcion'=>'PERMISO ESPECIAL DE PERMANENCIA','sigla'=>'PE'],
            ['descripcion'=>'DOCUMENTO EXTRANJERO','sigla'=>'DE']
        ]);
    }
}

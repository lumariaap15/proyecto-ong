<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVersionCasosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('version_casos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero_radicado');
            $table->integer('tabla_id')->unsigned()->nullable();
            $table->string('nombre_tabla');
            $table->enum('accion',['create','update','delete']);
            $table->text('valor_antes')->nullable();
            $table->text('valor_despues')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->enum('estado',['aprobado','cancelado','pendiente'])->default('pendiente');
            $table->text('comentario')->nullable();
            $table->dateTime('fecha');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('version_casos');
    }
}

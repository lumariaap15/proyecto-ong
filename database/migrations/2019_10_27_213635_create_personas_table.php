<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('tipo_documento_id');
            $table->foreign('tipo_documento_id')
                ->references('id')
                ->on('tipo_documentos')
                ->onDelete('cascade');
            $table->string('n_documento');
            $table->string('nombres');
            $table->string('apellido')->nullable();
            $table->date('fecha_nacimiento');
            $table->unsignedBigInteger('estado_civil_id');
            $table->foreign('estado_civil_id')
                ->references('id')
                ->on('estado_civils')
                ->onDelete('cascade');
            $table->unsignedBigInteger('grado_escolaridad_id');
            $table->foreign('grado_escolaridad_id')
                ->references('id')
                ->on('grado_escolaridads')
                ->onDelete('cascade');
            $table->enum('genero',['masculino','femenino']);
            $table->string('municipio_codigo')->index();
            $table->foreign('municipio_codigo')
                ->references('codigo')
                ->on('municipios')
                ->onDelete('cascade');
            $table->string('departamento_codigo')->index();
            $table->foreign('departamento_codigo')
                ->references('codigo')
                ->on('departamentos')
                ->onDelete('cascade');
            $table->string('direccion')->nullable();
            $table->unsignedBigInteger('grupo_etnico_id');
            $table->foreign('grupo_etnico_id')
                ->references('id')
                ->on('grupo_etnicos')
                ->onDelete('cascade');
            $table->unsignedBigInteger('grupo_indigena_id')->nullable();
            $table->foreign('grupo_indigena_id')
                ->references('id')
                ->on('grupo_indigenas')
                ->onDelete('cascade');
            $table->string('ocupacion');
            $table->boolean('lgtbi')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}

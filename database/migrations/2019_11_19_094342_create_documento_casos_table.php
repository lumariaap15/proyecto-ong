<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentoCasosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documento_casos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->text('descripcion');
            $table->string('archivo');
            $table->enum('tipo_documento', ['PDF', 'WORD', 'EXCEL', 'AUDIO', 'VIDEO','IMAGEN']);
            $table->unsignedBigInteger('observacion_id')->nullable();
            $table->unsignedBigInteger('caso_id')->nullable();

            $table->foreign('observacion_id')->references('id')->on('observacion_casos')
                ->onDelete('cascade');
            $table->foreign('caso_id')->references('id')->on('casos')
                ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documento_casos');
    }
}

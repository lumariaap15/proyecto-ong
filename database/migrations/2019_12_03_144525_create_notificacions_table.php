<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificacions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('mensaje');
            $table->date('fecha');
            $table->string('radicado');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('user_target')->nullable();
            $table->unsignedBigInteger('caso_id')->nullable();
            $table->enum('visto',['si','no'])->default('no');
            $table->unsignedBigInteger('version_caso_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificacions');
    }
}

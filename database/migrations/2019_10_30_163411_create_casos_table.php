<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCasosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('radicado');
            $table->unsignedBigInteger('denunciante_id');
            $table->unsignedBigInteger('victima_id')->nullable();
            $table->unsignedBigInteger('organizacion_victima_id')->nullable();
            $table->unsignedBigInteger('comunidad_id')->nullable();
            $table->unsignedBigInteger('hecho_victimizante_id');
            $table->date('fecha_de_recepcion');
            $table->unsignedBigInteger('responsable_del_hecho_id');
            $table->string('departamento_codigo')->index();
            $table->string('municipio_id');
            $table->string('direccion')->nullable();
            $table->text('relato');
            $table->integer('cantidad_personas_afectadas');
            $table->unsignedBigInteger('estado_proceso_caso_id');
            $table->foreign('denunciante_id')->references('id')->on('personas')
                ->onDelete('cascade');
            $table->foreign('victima_id')->references('id')->on('personas')
                ->onDelete('cascade');
            $table->foreign('organizacion_victima_id')->references('id')->on('organizacions')
            ->onDelete('cascade');
            $table->foreign('comunidad_id')->references('id')->on('comunidads')
                ->OnDelete('cascade');
            $table->foreign('hecho_victimizante_id')->references('id')->on('hecho_victimizantes')
                ->onDelete('cascade');
            $table->foreign('departamento_codigo')->references('codigo')->on('departamentos')
                ->onDelete('cascade');
            $table->foreign('municipio_id')->references('codigo')->on('municipios')
                ->onDelete('cascade');
            $table->foreign('responsable_del_hecho_id')->references('id')->on('responsable_hechos')
                ->onDelete('cascade');
            $table->foreign('estado_proceso_caso_id')->references('id')->on('estado_proceso_casos')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('casos');
    }
}

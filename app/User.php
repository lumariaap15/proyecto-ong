<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class User extends Authenticatable  implements JWTSubject, AuditableContract
{
    use Notifiable;
    use Auditable, SoftDeletes;


    const ROLE_ADMIN = 'administrador';
    const ROLE_CONSULTA = 'consulta';
    const ROLE_CASOS = 'casos';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $filasBuscar = [
        'name',
        'email',
    ];


    public function scopeBuscarCoincidencia($query, $termino)
    {
        return $query->whereLike($this->filasBuscar, $termino);
    }

    public  function  getJWTIdentifier() {
        return  $this->getKey();
    }

    public  function  getJWTCustomClaims() {
        return [];
    }


    public function hasRole($role){
        return $this->role == $role;
    }
}

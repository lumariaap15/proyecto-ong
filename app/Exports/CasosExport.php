<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CasosExport implements WithHeadings, FromCollection
{
    private $data;
    use Exportable;

    /**
     * CasosExport constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function headings(): array
    {
        return ['ID', 'Identificación de la victima', 'Nombre de la victima', 'Apellido de la victima', 'Género de la victima',
            'LGTBI victima','Ocupación victima','grado escolaridad victima',
            'Identificación del denunciante', 'Nombre del denunciante', 'Apellido del denunciante', 'Género del denunciante',
            'LGTBI Denunciante', 'Ocupacion denunciante','grado escolaridad denunciante', 'Departamento',
            'Municipio', 'Comunidad', 'Tipo de Liderazgo', 'Responsable del hecho', 'Hecho victimizante', 'Fecha caso'];
    }

    public function collection()
    {
        return collect([
            'data' => $this->data
        ]);
    }

}

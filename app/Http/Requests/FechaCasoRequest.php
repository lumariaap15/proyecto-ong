<?php

namespace App\Http\Requests;

use App\Traits\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class FechaCasoRequest extends FormRequest
{
    use RequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST') {
            return [
                'fecha_inicio' => 'date',
                'fecha_final' => "date",
            ];
        }
        if ($this->method() == 'PUT') {
            return [
                'fecha_inicio' => 'nullable|date',
                'fecha_final' => "nullable|date",
            ];
        }
    }
}

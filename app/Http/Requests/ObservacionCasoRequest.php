<?php

namespace App\Http\Requests;

use App\Traits\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class ObservacionCasoRequest extends FormRequest
{
    use RequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST') {
            return [
                'descripcion' => 'string',
                'caso_id' => 'numeric',
            ];
        }
        if ($this->method() == 'PUT') {
            return [
                'descripcion' => 'string',
                'caso_id' => 'numeric',
            ];
        }
    }
}

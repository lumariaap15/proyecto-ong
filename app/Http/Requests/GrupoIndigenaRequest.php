<?php

namespace App\Http\Requests;

use App\Traits\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class GrupoIndigenaRequest extends FormRequest
{
    use RequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('grupo_indigena');
        return [
            'descripcion' => "required|string|max:255|unique:grupo_indigenas,descripcion,{$id},id,deleted_at,NULL"
        ];
    }
}

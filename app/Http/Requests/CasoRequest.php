<?php

namespace App\Http\Requests;

use App\Traits\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class CasoRequest extends FormRequest
{
    use RequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //$id = $this->route('caso');
        if ($this->method() == 'POST') {
            return [
                'denunciante_id' => "required|numeric",
                'organizacion_victima_id' => "required|numeric",
                'tipo_victima' => "required|string"
            ];
        }
        if ($this->method() == 'PUT'){
            return[
                'denunciante_id' => "numeric",
                'tipo_victima' => "string"
            ];
        }
    }
}

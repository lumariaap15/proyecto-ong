<?php

namespace App\Http\Requests;

use App\Traits\RequestTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class TipoDocumentoRequest extends FormRequest
{
    use RequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $id = $this->route('tipo_documento');
        if($this->method() == 'POST'){
            return [
                'sigla'=>'required|size:2|unique:tipo_documentos,sigla,{$id},id,deleted_at,NULL',
                'descripcion'=>'required'
            ];
        }
        if($this->method() == 'PUT'){
            return [
                'sigla'=>"required|size:2|unique:tipo_documentos,sigla,{$id},id,deleted_at,NULL",
                'descripcion'=>'required'
            ];
        }

    }



}

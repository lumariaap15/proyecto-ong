<?php

namespace App\Http\Requests;

use App\Traits\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class GradoEscolaridadRequest extends FormRequest
{
   use RequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('grado_escolaridade');
        return [
            'descripcion' => "required|string|max:255|unique:grado_escolaridads,descripcion,{$id},id,deleted_at,NULL"
        ];
    }
}

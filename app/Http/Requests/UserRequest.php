<?php

namespace App\Http\Requests;

use App\Traits\RequestTrait;
use App\User;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    use RequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('usuario');
        if($this->method() == 'POST')
        {
            return [
                'name' => 'required|max:255',
                'email' => "required|email|max:255|unique:users,email,{$id},id,deleted_at,NULL",
                'role' => 'required|in:'.User::ROLE_ADMIN.','.User::ROLE_CASOS.','.User::ROLE_CONSULTA,
                'password'=> 'required|min:6|max:12'
            ];
        }

        if($this->method() == 'PUT')
        {
            return [
                'name' => 'max:255',
                'email' => "email|max:255|unique:users,email,{$id},id,deleted_at,NULL",
                'role' => 'in:'.User::ROLE_ADMIN.','.User::ROLE_CASOS.','.User::ROLE_CONSULTA,
            ];
        }

    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EstadoProcesoCasoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('estado_procesos');
        if ($this->method() == 'POST'){
            return [
                'descripcion' => "required|string|max:255|unique:estado_proceso_casos,descripcion,{$id},id,deleted_at,NULL"
            ];
        }
        if ($this->method() == 'PUT'){
            return [
                'descripcion' => "required|string|max:255|unique:estado_proceso_casos,descripcion,{$id},id,deleted_at,NULL"
            ];
        }
    }
}

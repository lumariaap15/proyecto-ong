<?php

namespace App\Http\Requests;

use App\Traits\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class ComunidadRequest extends FormRequest
{

    use RequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST'){
            return [
                'descripcion' => 'nullable|string|max:255',
                'municipio_codigo' => "nullable|string|exists:municipios,codigo",
            ];
        }
        if ($this->method() == 'PUT'){
            return [
                'descripcion' => 'required|string|max:255',
                'municipio_codigo' => "required|string|exists:municipios,codigo",
            ];
        }
    }
}

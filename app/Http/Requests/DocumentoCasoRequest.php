<?php

namespace App\Http\Requests;

use App\Traits\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class DocumentoCasoRequest extends FormRequest
{
    use RequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST') {
            return [
                'nombre' => 'string|required',
                'descripcion' => 'string|required',
                'archivo' => 'file|required',
                'caso_id' => 'nullable|numeric',
                'observacion_id' => 'nullable|numeric',
                'tipo_documento' => 'required|in:PDF,WORD,EXCEL,AUDIO,VIDEO,IMAGEN'
            ];
        }
        if ($this->method() == 'PUT') {
            return [
                'nombre' => 'string',
                'descripcion' => 'string',
                'archivo' => 'nullable|file',
                'caso_id' => 'nullable|numeric',
                'observacion_id' => 'nullable|numeric',
                'tipo_documento' => 'required|in:PDF,WORD,EXCEL,AUDIO,VIDEO,IMAGEN'
            ];
        }
    }
}

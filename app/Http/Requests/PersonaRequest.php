<?php

namespace App\Http\Requests;

use App\Traits\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class PersonaRequest extends FormRequest
{

    use RequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('persona');
        if($this->method() == 'POST'){
            return [
                'tipo_documento_id' => "required|numeric|exists:tipo_documentos,id",
                'n_documento' => "required|numeric|unique:personas,n_documento,null,id,deleted_at,NULL",
                'nombres'=>"required|string|max:255",
                'apellido'=>"required|string|max:255",
                'fecha_nacimiento' => 'required',
                'estado_civil_id' => "required|numeric|exists:estado_civils,id",
                'grado_escolaridad_id' => "required|numeric|exists:grado_escolaridads,id",
                'genero' => 'required',
                'municipio_codigo' => "required|string|exists:municipios,codigo",
                'departamento_codigo' => "required|string",
                'grupo_etnico_id' => "required|numeric|exists:grupo_etnicos,id",
                'grupo_indigena_id' => "nullable|numeric|exists:grupo_indigenas,id",
                'ocupacion' => 'string|max:255',
                'dereccion' => 'string|max:255',
                'lgtbi' => 'in:0,1'
            ];
        }

        if($this->method == 'PUT'){
            return [
                'tipo_documento_id' => "required|numeric|exists:tipo_documentos,id",
                'n_documento' => "required|numeric|unique:personas,n_documento,{$id},id,deleted_at,NULL",
                'nombres'=>"required|string|max:255",
                'apellido'=>"required|string|max:255",
                'fecha_nacimiento' => 'required',
                'estado_civil_id' => "required|numeric|exists:estado_civils,id",
                'grado_escolaridad_id' => "required|numeric|exists:grado_escolaridads,id",
                'genero' => 'required',
                'municipio_codigo' => "required|string|exists:municipios,codigo",
                'departamento_codigo' => "required|string",
                'grupo_etnico_id' => "required|numeric|exists:grupo_etnicos,id",
                'grupo_indigena_id' => "nullable|numeric|exists:grupo_indigenas,id",
                'ocupacion' => 'string|max:255',
                'dereccion' => 'string|max:255',
                'lgtbi' => 'in:0,1'
            ];
        }

    }
}

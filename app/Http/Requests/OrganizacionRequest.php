<?php

namespace App\Http\Requests;

use App\Traits\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class OrganizacionRequest extends FormRequest
{
    use RequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('organizacione');
        if ($this->method() == 'POST') {
            return [
                'descripcion' => "required|string|max:255|unique:organizacions,descripcion,{$id},id,deleted_at,NULL"
            ];
        }
        if ($this->method() == 'PUT'){
            return[
                'descripcion' => "required|string|max:255|unique:organizacions,descripcion,{$id},id,deleted_at,NULL"
            ];
        }

    }
}

<?php

namespace App\Http\Requests;

use App\Traits\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class EstadoCivilRequest extends FormRequest
{
    use RequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('estado_civile');
        return [
            'descripcion' => "required|string|max:255|unique:estado_civils,descripcion,{$id},id,deleted_at,NULL"
        ];
    }
}

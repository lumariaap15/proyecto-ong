<?php

namespace App\Http\Requests;

use App\Traits\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class ResponsableHechoRequest extends FormRequest
{
    use RequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('responsableHecho');
        if ($this->method() == 'POST') {
            return [
                'descripcion' => "required|string|max:200|unique:responsable_hechos,descripcion,{$id},id"
            ];
        }
        if ($this->method() == 'PUT') {
            return [
                'descripcion' => "required|string|max:200|unique:responsable_hechos,descripcion,{$id},id"
            ];
        }
    }
}

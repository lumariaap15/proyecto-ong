<?php

namespace App\Http\Controllers;

use App\Models\Notificacion;
use App\User;
use Illuminate\Http\Request;

class NotificacionController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.verify');
    }

    /**
     * @OA\Get(
     *     tags={"notificacion"},
     *     path="/api/notifiaciones",
     *     summary="Muestra todas las notificaciones",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="visto",
     *          description="el estado de la notificacion, 'si','no'",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="id",
     *          description="id del usuario que se quiere traer sus notificaciones",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar los casos."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index(Request $request)
    {
        if (auth()->user()->hasRole(User::ROLE_ADMIN) || auth()->user()->hasRole(User::ROLE_CASOS)) {
            $visto = $request->has('visto') ? $request->get('visto') : '';
            $id = $request->has('id') ? $request->get('id') : '';
            $notificaciones = Notificacion::with('user')
                ->where(function ($query) use ($visto) {
                    if ($visto != '') {
                        $query->where('visto', $visto);
                    }
                })
                ->where(function ($query) use ($id) {
                    if(auth()->user()->hasRole(User::ROLE_ADMIN)){
                        $query->where('user_target', null);
                    }else{
                        if ($id != '') {
                            $query->where('user_target', $id);
                        }
                    }

                })
                ->latest()
                ->get();
            return response()->json($notificaciones);
        } else {
            return response()->json(['error' => 'This action is unauthorized.'], 403);
        }
    }


    /**
     * @OA\Put(
     *     tags={"notificacion"},
     *     path="/api/notificaciones/{id}",
     *     summary="Permite cambiar estado una notificacion",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id de la version que se desea gestionar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="visto", type="string", example="si,no"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Version de caso modificada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        try {
            if (auth()->user()->hasRole(User::ROLE_ADMIN || auth()->user()->hasRole(User::ROLE_CASOS))) {
                $notificacion = Notificacion::query()->findOrFail($id);
                $notificacion->visto = $request->visto;
                $notificacion->save();
                return response()->json($notificacion);
            } else {
                return response()->json(['error' => 'This action is unauthorized.'], 403);
            }
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }


    /**
     * @OA\Put(
     *     tags={"notificacion"},
     *     path="/api/notificaciones/allvisto",
     *     summary="pasa todas las notifiaciones no vistas a vistas",
     *     security={{"bearer": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Version de caso modificada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function updateAllvisto()
    {
        try {
            if (auth()->user()->hasRole(User::ROLE_ADMIN) || auth()->user()->hasRole(User::ROLE_CASOS)) {
                $notificacion = Notificacion::query()->where('visto', 'no')->update([
                    'visto' => 'si'
                ]);
                return response()->json($notificacion);
            } else {
                return response()->json(['error' => 'This action is unauthorized.'], 403);
            }
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\GradoEscolaridadRequest;
use App\Models\GradoEscolaridad;
use App\Models\Persona;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class GradoEscolaridadController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['index', 'show']]);
    }

    /**
     * @OA\Get(
     *     tags={"grado_escolaridad"},
     *     path="/api/grado_escolaridades",
     *     summary="Muestra todos los grados de escolaridad",
     *     @OA\Parameter(
     *          name="page",
     *          description="numero de pagina, enviar solo si se desea paginar, de lo contrario regresa todos los registros",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="termino",
     *          description="descripcion del grado de escolaridad",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar todos los grados de escolaridad."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index()
    {
        try {
            $termino = \request()->has('termino') ? \request()->get('termino') : '';
            if (\request()->has('page')) {
                $gradoEscolaridades = GradoEscolaridad::query()->where(function ($query) use ($termino) {
                    if ($termino != '') {
                        $query->where('descripcion', 'like', '%' . $termino . '%');
                    }
                })->paginate(10);
            } else {
                $gradoEscolaridades = GradoEscolaridad::query()->where(function ($query) use ($termino) {
                    if ($termino != '') {
                        $query->where('descripcion', 'like', '%' . $termino . '%');
                    }
                })->get();
            }
            return response()->json($gradoEscolaridades, 200);
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     tags={"grado_escolaridad"},
     *     path="/api/grado_escolaridades",
     *     summary="Crea grado de escolaridad",
     *     security={{"bearer": {}}},
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="Profesional"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="grado de escolaridad creada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param GradoEscolaridadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(GradoEscolaridadRequest $request)
    {
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $gradoEscolaridad = new GradoEscolaridad();
                $gradoEscolaridad->fill($request->all());
                $gradoEscolaridad->save();
                return response()->json($gradoEscolaridad, 201);
            } catch (\Exception $exception) {
                return response()->json(['error' => $exception->getMessage()], 500);
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }

    /**
     * @OA\Put(
     *     tags={"grado_escolaridad"},
     *     path="/api/grado_escolaridades/{id}",
     *     summary="actualizar un grado de escolaridad",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del grado de escolaridad a actualizar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="Bachiller"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="grado de escolaridad actualizado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param GradoEscolaridadRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(GradoEscolaridadRequest $request, $id)
    {
        $respuesta = null;
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $gradoEscolaridad = GradoEscolaridad::query()->findOrFail($id);
                $gradoEscolaridad->fill($request->all());
                $gradoEscolaridad->save();
                $respuesta = response()->json($gradoEscolaridad, 200);
            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }

    /**
     * @OA\Get(
     *     tags={"grado_escolaridad"},
     *     path="/api/grado_escolaridades/{id}",
     *     summary="Retorna un grado de escolaridad especifico",
     *     @OA\Parameter(
     *          name="id",
     *          description="id del grado de escolaridad a mostrar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Retonar el grado de escolaridad."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $respuesta = null;
        try {
            $gradoEscolaridad = GradoEscolaridad::query()->findOrFail($id);
            $respuesta = response()->json($gradoEscolaridad);
        } catch (ModelNotFoundException $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 500);
        } finally {
            return $respuesta;
        }
    }

    /**
     * @OA\Delete(
     *     tags={"grado_escolaridad"},
     *     path="/api/grado_escolaridades/{id}",
     *     summary="Elimina un grado de escolaridad",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del grado de escolaridad a eliminar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="grado de escolaridad Eliminado."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $respuesta = null;
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $gradoEscolaridad = GradoEscolaridad::query()->findOrFail($id);
                $extisPersona = Persona::query()->where('grado_escolaridad_id', $id)->exists();
                if (!$extisPersona) {
                    $gradoEscolaridad->delete();
                    $respuesta = response()->json($gradoEscolaridad);
                } else {
                    $respuesta = response()->json(['error' => 'No se puede eliminar'], 400);
                }
            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }

    }
}

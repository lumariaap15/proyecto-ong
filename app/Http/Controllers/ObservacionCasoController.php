<?php

namespace App\Http\Controllers;

use App\Http\Requests\ObservacionCasoRequest;
use App\Models\Caso;
use App\Models\ObservacionCaso;
use App\Models\VersionCaso;
use App\User;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ObservacionCasoController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['index', 'show']]);

    }

    /**
     * @OA\Get(
     *     tags={"Observacion Casos"},
     *     path="/api/observacionCaso/caso/{idcaso}",
     *     summary="Muestra todas las observaciones de un caso especifico",
     *     @OA\Parameter(
     *          name="id",
     *          description="id del caso que se quiere ver las observaciones",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="termino",
     *          description="Termino de busqueda para buscar dentro de las observaciones si vien sea por el nombre de usuario o descripcion",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar todos los estado civiles."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index(Request $request,$idcaso)
    {
        try {
            $termino = $request->has('termino')?$request->get('termino'):'';
            $observaciones = ObservacionCaso::query()
                ->with('user')
                ->where('caso_id', $idcaso)
                ->where(function($query) use ($termino){
                    if($termino != ""){
                        $query->where('descripcion','like','%'.$termino.'%');
                    }
                })
                ->where(function($query) use ($termino){
                    if($termino != ''){
                        $query->whereHas('user',function ($query2) use ($termino){
                           $query2->where('name','like','%'.$termino.'%');
                        });
                    }
                })
                ->get();
            return response()->json($observaciones, 200);
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     tags={"Observacion Casos"},
     *     path="/api/observacionCaso",
     *     summary="Agregar una observacion a un caso",
     *     security={{"bearer": {}}},
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="caso_id", type="numeric", example="1"),
     *         @OA\Property(property="descripcion", type="string", example="Observacion del caso"),
     *         @OA\Property(property="comentario", type="string", example="Cometario actualizacion, solo aplica a usuario caso"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="Observacion agregada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param ObservacionCasoRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ObservacionCasoRequest $request)
    {
        DB::beginTransaction();
        if (auth()->user()->hasRole(User::ROLE_ADMIN) || auth()->user()->hasRole(User::ROLE_CASOS)) {
            try {
                if(auth()->user()->hasRole(User::ROLE_ADMIN)){
                    $observacion = new ObservacionCaso();
                    $observacion->fill($request->all());
                    $observacion->user_id = auth()->id();
                    $observacion->save();
                    DB::commit();
                    return response()->json($observacion, 201);
                }else{
                    $caso = Caso::query()->findOrFail($request->caso_id);
                    $observacion = (object)[];
                    $observacion->descripcion = $request->descripcion;
                    $observacion->user_id = auth()->id();
                    $observacion->caso_id = $request->caso_id;
                    $radicado = $caso->radicado;
                    $version = $this->storeVersion($radicado, 'observacion_casos', null, 'create', null, $observacion,$request->comentario);
                    $this->storeNotificacion($radicado, 'El usuario ' . auth()->user()->name . ' agregó una observación al caso ' . $radicado, $version->id);
                    DB::commit();
                    return response()->json($observacion, 201);
                }
            } catch (\Exception $exception) {
                DB::rollBack();
                return response()->json([
                    'error' => $exception->getMessage()
                ], 500);
            }
        } else {
            DB::rollBack();
            return response()->json(['error' => 'This action is unauthorized.'], 403);
        }
    }

    /**
     * @OA\Put(
     *     tags={"Observacion Casos"},
     *     path="/api/observacionCaso/{id}",
     *     summary="actualizar una observación",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id de la observación a actualizar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="caso_id", type="integer", example="1"),
     *         @OA\Property(property="descripcion", type="string", example="Nueva descripcion"),
     *         @OA\Property(property="comentario", type="string", example="Cometario actualizacion, solo aplica a usuario caso"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Observcaión actualizada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ObservacionCasoRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            if (auth()->user()->hasRole(User::ROLE_ADMIN) || auth()->user()->hasRole(User::ROLE_CASOS)) {
                $observacion = ObservacionCaso::query()->findOrFail($id);
                if(auth()->user()->hasRole(User::ROLE_ADMIN)){
                    $observacion->fill($request->all());
                    $observacion->user_id = auth()->id();
                    $observacion->save();
                }else{
                    //$caso = Caso::query()->findOrFail($request->caso_id);
                    $valor_antes = (object)[];
                    $valor_despues = (object)[];
                    $radicado = $observacion->caso->radicado;
                    $existe = VersionCaso::query()
                        ->where('numero_radicado',$radicado)
                        ->where('nombre_tabla','observacion_casos')
                        ->where('tabla_id',$observacion->id)
                        //->where('accion','update')
                        ->where('estado','pendiente')
                        ->exists();
                    if($existe){
                        return  response()->json(['error'=>'Existe una actualizacion pendiente por aprobacion para esta observación'],400);
                    }
                    if($request->has('descripcion') && $request->filled('descripcion') && ($observacion->descripcion != $request->descripcion)){
                        $valor_antes->descripcion = $observacion->descripcion;
                        $valor_despues->descripcion = $request->descripcion;
                    }
                    if($request->has('caso_id') && $request->filled('caso_id') && ($observacion->caso_id != $request->caso_id)){
                        $valor_antes->caso_id = $observacion->caso_id;
                        $valor_despues->caso_id = $request->caso_id;
                    }
                    if($observacion->user_id != auth()->id()){
                        $valor_antes->user_id = $observacion->user_id;
                        $valor_despues->user_id = auth()->id();
                    }

                    $version = $this->storeVersion($radicado, 'observacion_casos', $observacion->id, 'update', $valor_antes, $valor_despues,$request->comentario);
                    $this->storeNotificacion($radicado, 'El usuario ' . auth()->user()->name . ' actualizó una observación del caso ' . $radicado, $version->id);
                }
                DB::commit();
                return response()->json($observacion, 200);
            }else{
                DB::rollBack();
                return response()->json(['error' => 'This action is unauthorized.'], 403);
            }
        } catch (ModelNotFoundException $exception) {
            DB::rollBack();
            return response()->json([
                'error' => $exception->getMessage()
            ], 404);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Delete(
     *     tags={"Observacion Casos"},
     *     path="/api/observacionCaso/{id}",
     *     summary="Elimina una observación",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id de la observación a eliminar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="comentario", type="string", example="Cometario actualizacion, solo aplica a usuario caso"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Observación Eliminada."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request,$id)
    {
        DB::beginTransaction();
        try {
            if (auth()->user()->hasRole(User::ROLE_ADMIN) || auth()->user()->hasRole(User::ROLE_CASOS)) {
                $observacion = ObservacionCaso::query()->findOrFail($id);
                if(auth()->user()->hasRole(User::ROLE_ADMIN)){
                    $observacion->delete();
                }else{
                    $radicado = $observacion->caso->radicado;
                    $existe = VersionCaso::query()
                        ->where('numero_radicado',$radicado)
                        ->where('nombre_tabla','observacion_casos')
                        ->where('tabla_id',$observacion->id)
                        //->where('accion','delete')
                        ->where('estado','pendiente')
                        ->exists();
                    if($existe){
                        return  response()->json(['error','Existe una actualización pendiente para esta observación'],400);
                    }
                    $version = $this->storeVersion($radicado, 'observacion_casos', $observacion->id, 'delete', $observacion, null,$request->comentario);
                    $this->storeNotificacion($radicado, 'El usuario ' . auth()->user()->name . ' eliminó una observación del caso ' . $radicado, $version->id);
                }
                DB::commit();
                return response()->json($observacion, 200);
            } else {
                DB::rollBack();
                return response()->json(['error' => 'This action is unauthorized.'], 403);
            }
        } catch
        (ModelNotFoundException $exception) {
            DB::rollBack();
            return response()->json([
                'error' => $exception->getMessage()
            ], 404);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }

    }
}


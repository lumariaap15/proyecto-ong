<?php

namespace App\Http\Controllers;

use App\Exports\CasosExport;
use App\Models\Caso;
use App\Models\Municipio;
use App\Models\Persona;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;


class ReportesController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify')->except('getReporteGeneral');
    }

    /**
     * @OA\Get(
     *     tags={"Reportes"},
     *     path="/api/reporte/municipios",
     *     summary="Reportes por municipios",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="fecha_de_inicio",
     *          description="Fecha del caso",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="fecha_final",
     *          description="Fecha del caso ",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar los casos."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function getReportePorMunicipios(Request $request)
    {
        try {
            $fecha_de_inicio = $request->has('fecha_de_inicio') ? $request->get('fecha_de_inicio') : '';
            $fecha_final = $request->has('fecha_final') ? $request->get('fecha_final') : '';
            $caso = Municipio::query()
                ->withCount('casos')
                ->where(function ($query) use ($fecha_de_inicio, $fecha_final) {
                    $query->whereHas('casos', function ($query2) use ($fecha_de_inicio, $fecha_final){
                        $query2->whereHas('fechas', function ($query2) use ($fecha_de_inicio, $fecha_final) {
                            $fecha_de_inicio = Carbon::createFromFormat('Y-m-d', $fecha_de_inicio);
                            $fecha_final = Carbon::createFromFormat('Y-m-d', $fecha_final);
                            $query2->whereDate('fecha_inicio', '>=', $fecha_de_inicio);
                            $query2->whereDate('fecha_final', '<=', $fecha_final);
                        });
                    });
                })
                ->get();


            return response()->json($caso, 201);
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Get(
     *     tags={"Reportes"},
     *     path="/api/reporte/hechosVictimizantes",
     *     summary="Reportes por Hechos Victimizante",
     *     security={{"bearer": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar los casos."
     *     ),
     *     @OA\Parameter(
     *          name="departamento",
     *          description="codigo del departamento a buscar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="municipio",
     *          description="codigo del municipio a buscar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="fecha_de_inicio",
     *          description="Fecha del caso",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="fecha_final",
     *          description="Fecha del caso ",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function getReporteHechosVictimizante(Request $request)
    {
        try {
            $departamento = $request->has('departamento') ? $request->get('departamento') : '';
            $municipio = $request->has('municipio') ? $request->get('municipio') : '';
            $fecha_de_inicio = $request->has('fecha_de_inicio') ? $request->get('fecha_de_inicio') : '';
            $fecha_final = $request->has('fecha_final') ? $request->get('fecha_final') : '';

            $caso = Caso::join('hecho_victimizantes', 'casos.hecho_victimizante_id', 'hecho_victimizantes.id')
                ->select('hecho_victimizantes.descripcion as hecho_victimizante',
                    DB::raw('count(casos.hecho_victimizante_id) as numeroHechos'))
                ->groupBy('hecho_victimizantes.descripcion')
                ->where(function ($query) use ($municipio) {
                    if ($municipio != '') {
                        $query->where('municipio_id', $municipio);
                    }
                })
                ->where(function ($query) use ($departamento) {
                    if ($departamento != '') {
                        $query->where('departamento_codigo', $departamento);
                    }
                })->where(function ($query) use ($fecha_de_inicio, $fecha_final) {
                    if ($fecha_de_inicio != '' && $fecha_final != '') {
                        $query->whereHas('fechas', function ($query2) use ($fecha_de_inicio, $fecha_final) {
                            $fecha_de_inicio = Carbon::createFromFormat('Y-m-d', $fecha_de_inicio);
                            $fecha_final = Carbon::createFromFormat('Y-m-d', $fecha_final);
                            $query2->whereDate('fecha_inicio', '>=', $fecha_de_inicio);
                            $query2->whereDate('fecha_final', '<=', $fecha_final);
                        });
                    }
                })->get();
            foreach ($caso as $casos) {
                $totalCasos = $caso;
            }
            return response()->json($caso, 201);
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Get(
     *     tags={"Reportes"},
     *     path="/api/reporte/hechosVictimizantesComparativo",
     *     summary="Reportes comparativos",
     *  security={{"bearer": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar los casos."
     *     ),
     *     @OA\Parameter(
     *          name="departamento",
     *          description="codigo del departamento a buscar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="municipio",
     *          description="codigo del municipio a buscar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="fecha_de_inicio",
     *          description="Fecha del caso",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="fecha_final",
     *          description="Fecha del caso ",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="fecha_de_inicio2",
     *          description="Fecha del caso",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="fecha_final2",
     *          description="Fecha del caso ",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function getReporteHechosVictimizanteComparativo(Request $request)
    {
        try {
            $departamento = $request->has('departamento') ? $request->get('departamento') : '';
            $municipio = $request->has('municipio') ? $request->get('municipio') : '';
            $fecha_de_inicio = $request->has('fecha_de_inicio') ? $request->get('fecha_de_inicio') : '';
            $fecha_final = $request->has('fecha_final') ? $request->get('fecha_final') : '';
            $fecha_de_inicio2 = $request->has('fecha_de_inicio2') ? $request->get('fecha_de_inicio2') : '';
            $fecha_final2 = $request->has('fecha_final2') ? $request->get('fecha_final2') : '';

            $casosPeriodo1 = Caso::join('hecho_victimizantes', 'casos.hecho_victimizante_id', 'hecho_victimizantes.id')
                ->select('hecho_victimizantes.descripcion as hecho_victimizante', DB::raw('count(casos.hecho_victimizante_id) as numeroCasos'))
                ->groupBy('hecho_victimizantes.descripcion')
                ->where(function ($query) use ($municipio) {
                    if ($municipio != '') {
                        $query->where('municipio_id', $municipio);
                    }
                })
                ->where(function ($query) use ($departamento) {
                    if ($departamento != '') {
                        $query->where('departamento_codigo', $departamento);
                    }
                })
                ->where(function ($query) use ($fecha_de_inicio, $fecha_final) {
                    if ($fecha_de_inicio != '' && $fecha_final != '') {
                        $query->whereHas('fechas', function ($query2) use ($fecha_de_inicio, $fecha_final) {
                            $fecha_de_inicio = Carbon::createFromFormat('Y-m-d', $fecha_de_inicio);
                            $fecha_final = Carbon::createFromFormat('Y-m-d', $fecha_final);
                            $query2->whereDate('fecha_inicio', '>=', $fecha_de_inicio);
                            $query2->whereDate('fecha_final', '<=', $fecha_final);
                        });
                    }
                })->get();
            $casosPeriodo2 = Caso::join('hecho_victimizantes', 'casos.hecho_victimizante_id', 'hecho_victimizantes.id')
                ->select('hecho_victimizantes.descripcion as hecho_victimizante', DB::raw('count(casos.hecho_victimizante_id) as numeroCasos'))
                ->groupBy('hecho_victimizantes.descripcion')
                ->where(function ($query) use ($municipio) {
                    if ($municipio != '') {
                        $query->where('municipio_id', $municipio);
                    }
                })
                ->where(function ($query) use ($departamento) {
                    if ($departamento != '') {
                        $query->where('departamento_codigo', $departamento);
                    }
                })
                ->where(function ($query) use ($fecha_de_inicio2, $fecha_final2) {
                    if ($fecha_de_inicio2 != '' && $fecha_final2 != '') {
                        $query->whereHas('fechas', function ($query2) use ($fecha_de_inicio2, $fecha_final2) {
                            $fecha_de_inicio2 = Carbon::createFromFormat('Y-m-d', $fecha_de_inicio2);
                            $fecha_final2 = Carbon::createFromFormat('Y-m-d', $fecha_final2);
                            $query2->whereDate('fecha_inicio', '>=', $fecha_de_inicio2);
                            $query2->whereDate('fecha_final', '<=', $fecha_final2);
                        });
                    }
                })->get();

            $resultado = [];
            $totalPeriodo1 = 0;
            $totalPeriodo2 = 0;
            foreach ($casosPeriodo1 as $caso) {
                array_push($resultado, [
                    'hechoVictimizante' => $caso->hecho_victimizante,
                    'periodo1' => $caso->numeroCasos,
                    'periodo2' => 0,
                ]);
                $totalPeriodo1 += $caso->numeroCasos;
            }
            foreach ($casosPeriodo2 as $caso) {
                $yaExiste = false;
                $i = 0;
                while (!$yaExiste && ($i < count($resultado))) {
                    if ($resultado[$i]['hechoVictimizante'] === $caso->hecho_victimizante) {
                        $yaExiste = true;
                        $resultado[$i]['periodo2'] = $resultado[$i]['periodo2'] + intval($caso->numeroCasos);
                    }
                    $i++;
                }
                if (!$yaExiste) {
                    array_push($resultado, [
                        'hechoVictimizante' => $caso->hecho_victimizante,
                        'periodo1' => 0,
                        'periodo2' => $caso->numeroCasos,
                    ]);
                }
                $totalPeriodo2 += $caso->numeroCasos;
            }
            return response()->json(['hechoVictimizante' => $resultado, 'totalPeriodo1' => $totalPeriodo1, 'totalPeriodo2' => $totalPeriodo2], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Get(
     *     tags={"Reportes"},
     *     path="/api/reporte/genero",
     *     summary="Reportes por sexo",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="fecha_de_inicio",
     *          description="Fecha del caso",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="departamento",
     *          description="codigo del departamento a buscar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="municipio",
     *          description="codigo del municipio a buscar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="fecha_final",
     *          description="Fecha del caso ",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar los casos."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function getReportePorGenero(Request $request)
    {
        try {
            $departamento = $request->has('departamento') ? $request->get('departamento') : '';
            $municipio = $request->has('municipio') ? $request->get('municipio') : '';
            $fecha_de_inicio = $request->has('fecha_de_inicio') ? $request->get('fecha_de_inicio') : '';
            $fecha_final = $request->has('fecha_final') ? $request->get('fecha_final') : '';
            $victima = $request->has('victima') ? $request->get('victima') : '';

            $caso = Caso::join('personas', 'casos.victima_id', 'personas.id')->
            select('personas.genero as persona', DB::raw('count(casos.victima_id) as numero'))->groupBy('personas.genero')
                ->where(function ($query) use ($municipio) {
                    // dd($municipio);
                    if ($municipio != '') {
                        $query->where('municipio_id', $municipio);
                    }
                })
                ->where(function ($query) use ($departamento) {
                    if ($departamento != '') {
                        $query->where('casos.departamento_codigo', $departamento);
                    }
                })
                ->where(function ($query) use ($fecha_de_inicio, $fecha_final) {
                    if ($fecha_de_inicio != '' && $fecha_final != '') {
                        $query->whereHas('fechas', function ($query2) use ($fecha_de_inicio, $fecha_final) {
                            $fecha_de_inicio = Carbon::createFromFormat('Y-m-d', $fecha_de_inicio);
                            $fecha_final = Carbon::createFromFormat('Y-m-d', $fecha_final);
                            $query2->whereDate('fecha_inicio', '>=', $fecha_de_inicio);
                            $query2->whereDate('fecha_final', '<=', $fecha_final);
                        });
                    }
                })->get();
            return response()->json($caso, 201);
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Get(
     *     tags={"Reportes"},
     *     path="/api/reporte/responsablesHecho",
     *     summary="Reportes por responsables de hechos",
     *      security={{"bearer": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar los casos."
     *     ),
     *     @OA\Parameter(
     *          name="departamento",
     *          description="codigo del departamento a buscar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="municipio",
     *          description="codigo del municipio a buscar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="fecha_de_inicio",
     *          description="Fecha del caso",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="fecha_final",
     *          description="Fecha del caso ",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function getReporteResponsablesHecho(Request $request)
    {
        try {
            $departamento = $request->has('departamento') ? $request->get('departamento') : '';
            $municipio = $request->has('municipio') ? $request->get('municipio') : '';
            $fecha_de_inicio = $request->has('fecha_de_inicio') ? $request->get('fecha_de_inicio') : '';
            $fecha_final = $request->has('fecha_final') ? $request->get('fecha_final') : '';

            $caso = Caso::join('responsable_hechos', 'casos.responsable_del_hecho_id', 'responsable_hechos.id')
                ->select('responsable_hechos.descripcion as responsable_hecho', DB::raw('count(casos.responsable_del_hecho_id) as numeroHechos'))
                ->groupBy('responsable_hechos.descripcion')->where(function ($query) use ($municipio) {
                    if ($municipio != '') {
                        $query->where('municipio_id', $municipio);
                    }
                })
                ->where(function ($query) use ($departamento) {
                    if ($departamento != '') {
                        $query->where('departamento_codigo', $departamento);
                    }
                })->where(function ($query) use ($fecha_de_inicio, $fecha_final) {
                    if ($fecha_de_inicio != '' && $fecha_final != '') {
                        $query->whereHas('fechas', function ($query2) use ($fecha_de_inicio, $fecha_final) {
                            $fecha_de_inicio = Carbon::createFromFormat('Y-m-d', $fecha_de_inicio);
                            $fecha_final = Carbon::createFromFormat('Y-m-d', $fecha_final);
                            $query2->whereDate('fecha_inicio', '>=', $fecha_de_inicio);
                            $query2->whereDate('fecha_final', '<=', $fecha_final);
                        });
                    }
                })->get();
            return response()->json($caso, 201);
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Get(
     *     tags={"Reportes"},
     *     path="/api/reporte/hechosResponsablesHechoComparativo",
     *     summary="Reportes por responsables de hechos",
     *     security={{"bearer": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar los casos."
     *     ),
     *     @OA\Parameter(
     *          name="departamento",
     *          description="codigo del departamento a buscar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="municipio",
     *          description="codigo del municipio a buscar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="fecha_de_inicio",
     *          description="Fecha del caso",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="fecha_final",
     *          description="Fecha del caso ",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="fecha_de_inicio2",
     *          description="Fecha del caso",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="fecha_final2",
     *          description="Fecha del caso ",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function getReporteResponsablesHechoComparativo(Request $request)
    {
        try {
            $departamento = $request->has('departamento') ? $request->get('departamento') : '';
            $municipio = $request->has('municipio') ? $request->get('municipio') : '';
            $fecha_de_inicio = $request->has('fecha_de_inicio') ? $request->get('fecha_de_inicio') : '';
            $fecha_final = $request->has('fecha_final') ? $request->get('fecha_final') : '';
            $fecha_de_inicio2 = $request->has('fecha_de_inicio2') ? $request->get('fecha_de_inicio2') : '';
            $fecha_final2 = $request->has('fecha_final2') ? $request->get('fecha_final2') : '';

            $casosPeriodo1 = Caso::join('responsable_hechos', 'casos.responsable_del_hecho_id', 'responsable_hechos.id')
                ->select('responsable_hechos.descripcion as responsable_hecho', DB::raw('count(casos.responsable_del_hecho_id) as numeroHechos'))
                ->groupBy('responsable_hechos.descripcion')
                ->where(function ($query) use ($municipio) {
                    if ($municipio != '') {
                        $query->where('municipio_id', $municipio);
                    }
                })
                ->where(function ($query) use ($departamento) {
                    if ($departamento != '') {
                        $query->where('departamento_codigo', $departamento);
                    }
                })
                ->where(function ($query) use ($fecha_de_inicio, $fecha_final) {
                    if ($fecha_de_inicio != '' && $fecha_final != '') {
                        $query->whereHas('fechas', function ($query2) use ($fecha_de_inicio, $fecha_final) {
                            $fecha_de_inicio = Carbon::createFromFormat('Y-m-d', $fecha_de_inicio);
                            $fecha_final = Carbon::createFromFormat('Y-m-d', $fecha_final);
                            $query2->whereDate('fecha_inicio', '>=', $fecha_de_inicio);
                            $query2->whereDate('fecha_final', '<=', $fecha_final);
                        });
                    }
                })->get();

            $casosPeriodo2 = Caso::join('responsable_hechos', 'casos.responsable_del_hecho_id', 'responsable_hechos.id')
                ->select('responsable_hechos.descripcion as responsable_hecho', DB::raw('count(casos.responsable_del_hecho_id) as numeroHechos'))
                ->groupBy('responsable_hechos.descripcion')->where(function ($query) use ($municipio) {
                    if ($municipio != '') {
                        $query->where('municipio_id', $municipio);
                    }
                })
                ->where(function ($query) use ($departamento) {
                    if ($departamento != '') {
                        $query->where('departamento_codigo', $departamento);
                    }
                })->where(function ($query) use ($fecha_de_inicio2, $fecha_final2) {
                    if ($fecha_de_inicio2 != '' && $fecha_final2 != '') {
                        $query->whereHas('fechas', function ($query2) use ($fecha_de_inicio2, $fecha_final2) {
                            $fecha_de_inicio = Carbon::createFromFormat('Y-m-d', $fecha_de_inicio2);
                            $fecha_final = Carbon::createFromFormat('Y-m-d', $fecha_final2);
                            $query2->whereDate('fecha_inicio', '>=', $fecha_de_inicio);
                            $query2->whereDate('fecha_final', '<=', $fecha_final);
                        });
                    }
                })->get();
            $resultado = [];
            $totalPeriodo1 = 0;
            $totalPeriodo2 = 0;
            foreach ($casosPeriodo1 as $caso) {
                array_push($resultado, [
                    'responsable' => $caso->responsable_hecho,
                    'periodo1' => $caso->numeroHechos,
                    'periodo2' => 0,
                ]);
                $totalPeriodo1 += $caso->numeroHechos;
            }
            foreach ($casosPeriodo2 as $caso) {
                $yaExiste = false;
                $i = 0;
                while (!$yaExiste && ($i < count($resultado))) {
                    if ($resultado[$i]['responsable'] === $caso->responsable_hecho) {
                        $yaExiste = true;
                        $resultado[$i]['periodo2'] = $resultado[$i]['periodo2'] + intval($caso->numeroHechos);
                    }
                    $i++;
                }
                if (!$yaExiste) {
                    array_push($resultado, [
                        'responsable' => $caso->responsable_hecho,
                        'periodo1' => 0,
                        'periodo2' => $caso->numeroHechos,
                    ]);
                }
                $totalPeriodo2 += $caso->numeroHechos;
            }
            return response()->json(['resultado' => $resultado, 'totalPeriodo1' => $totalPeriodo1, 'totalPeriodo2' => $totalPeriodo2], 200);


        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Get(
     *     tags={"Reportes"},
     *     path="/api/reporte/liderazgo",
     *     summary="Reporte por liderazgo",
     *     security={{"bearer": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar los casos."
     *     ),
     *     @OA\Parameter(
     *          name="departamento",
     *          description="codigo del departamento a buscar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="municipio",
     *          description="codigo del municipio a buscar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="fecha_de_inicio",
     *          description="Fecha del caso",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="fecha_final",
     *          description="Fecha del caso ",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function getReportePorLiderazgo(Request $request)
    {
        try {
                $departamento = $request->has('departamento') ? $request->get('departamento') : '';
                $municipio = $request->has('municipio') ? $request->get('municipio') : '';
                $fecha_de_inicio = $request->has('fecha_de_inicio') ? $request->get('fecha_de_inicio') : '';
                $fecha_final = $request->has('fecha_final') ? $request->get('fecha_final') : '';

                $caso = Caso::join('organizacions', 'casos.organizacion_victima_id', 'organizacions.id')
                    ->select('organizacions.descripcion as organizacions', DB::raw('count(casos.organizacion_victima_id) as numeroHechos'))
                    ->groupBy('organizacions.descripcion')
                    ->where(function ($query) use ($municipio) {
                        if ($municipio != '') {
                            $query->where('municipio_id', $municipio);
                        }
                    })
                    ->where(function ($query) use ($departamento) {
                        if ($departamento != '') {
                            $query->where('departamento_codigo', $departamento);
                        }
                    })
                    ->where(function ($query) use ($fecha_de_inicio, $fecha_final) {
                        if ($fecha_de_inicio != '' && $fecha_final != '') {
                            $query->whereHas('fechas', function ($query2) use ($fecha_de_inicio, $fecha_final) {
                                $fecha_de_inicio = Carbon::createFromFormat('Y-m-d', $fecha_de_inicio);
                                $fecha_final = Carbon::createFromFormat('Y-m-d', $fecha_final);
                                $query2->whereDate('fecha_inicio', '>=', $fecha_de_inicio);
                                $query2->whereDate('fecha_final', '<=', $fecha_final);
                            });
                        }
                    })->get();
                return response()->json($caso, 201);
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Get(
     *     tags={"Reportes"},
     *     path="/api/reporte/general",
     *     summary="Reporte general",
     *     security={{"bearer": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar los casos."
     *     ),
     *    @OA\Parameter(
     *          name="departamento",
     *          description="codigo del departamento a buscar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="municipio",
     *          description="codigo del municipio a buscar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="fecha_de_inicio",
     *          description="Fecha del caso",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="fecha_final",
     *          description="Fecha del caso ",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function getReporteGeneral(Request $request)
    {
        $array = [];
        try {
            $fecha_de_inicio = $request->has('fecha_de_inicio') ? $request->get('fecha_de_inicio') : '';
            $fecha_final = $request->has('fecha_final') ? $request->get('fecha_final') : '';
            $departamento = $request->has('departamento') ? $request->get('departamento') : '';
            $municipio = $request->has('municipio') ? $request->get('municipio') : '';

            $caso = Caso::with(
                'victima:id,nombres,apellido,genero,n_documento,lgtbi,ocupacion,grado_escolaridad_id',
                'denunciante:id,nombres,apellido,genero,n_documento,lgtbi,ocupacion',
                'fechas',
                'municipios:codigo,descripcion',
                'organizacions:id,descripcion',
                'responsable:id,descripcion',
                'comunidad:id,descripcion',
                'hecho_victimizante:id,descripcion',
                'departamento:codigo,descripcion'

            )->with('victima.gradoEscolaridad:id,descripcion',
                'denunciante.gradoEscolaridad:id,descripcion')
                ->select('id', 'radicado', 'victima_id', 'municipio_id', 'departamento_codigo','denunciante_id', 'organizacion_victima_id',
                    'responsable_del_hecho_id', 'hecho_victimizante_id', 'comunidad_id')
                ->where(function ($query) use ($fecha_de_inicio, $fecha_final) {
                    if ($fecha_de_inicio != '' && $fecha_final != '') {
                        $query->whereHas('fechas', function ($query2) use ($fecha_de_inicio, $fecha_final) {
                            $fecha_de_inicio = Carbon::createFromFormat('Y-m-d', $fecha_de_inicio);
                            $fecha_final = Carbon::createFromFormat('Y-m-d', $fecha_final);
                            $query2->whereBetween('fecha_inicio', [$fecha_de_inicio, $fecha_final]);
                            $query2->orWhereBetween('fecha_final', [$fecha_de_inicio, $fecha_final]);
                        });
                    }
                })
                ->where(function ($query) use ($municipio) {
                    if ($municipio != '') {
                        $query->where('municipio_id', $municipio);
                    }
                })
                ->where(function ($query) use ($departamento) {
                    if ($departamento != '') {
                        $query->where('departamento_codigo', $departamento);
                    }
                })->get();


            foreach ($caso as $key => $cas) {
                if ($cas->victima_id != null) {
                    $escolaridad_victima = $cas->victima->gradoEscolaridad->descripcion;
                } else {
                    $escolaridad_victima = 'No aplica';
                }
                if ($cas->denunciante_id != null) {
                    $escolaridad_denunciante = $cas->denunciante->gradoEscolaridad->descripcion;
                } else {
                    $escolaridad_denunciante = 'No aplica';
                }
                $radicado = $cas->radicado;
                $identificacion = $cas->victima['n_documento'];
                $nombres = $cas->victima['nombres'];
                $apellido = $cas->victima['apellido'];
                $genero = $cas->victima['genero'];
                $lgtbi_victima = $cas->victima['lgtbi'];
                $ocupacion_victima = $cas->victima['ocupacion'];
                $identificacion_denunciante = $cas->denunciante['n_documento'];
                $nombres_denunciante = $cas->denunciante->nombres;
                $apellido_denunciante = $cas->denunciante->apellido;
                $genero_denunciante = $cas->denunciante->genero;
                $lgtbi_denunciante = $cas->denunciante ? $cas->denunciante->lgtbi : "";
                $ocupacion_denunciante = $cas->denunciante->ocupacion;
                $municipio = $cas->municipios->descripcion;
                $departamento = $cas->departamento->descripcion;
                $comunidad = $cas->comunidad ? $cas->comunidad->descripcion : "";
                $ong = $cas->organizacions ? $cas->organizacions->descripcion : "";
                $responsable = $cas->responsable->descripcion;
                $hechovictimisante = $cas->hecho_victimizante->descripcion;


                $periodos = "";
                $total_fechas = isset($cas->fechas) ? count($cas->fechas) - 1 : 0;

                foreach ($cas->fechas as $key2 => $fecha) {
                    if ($key2 < $total_fechas) {
                        $periodos .= $fecha->fecha_inicio . " a " . $fecha->fecha_final . ", ";
                    } else {
                        $periodos .= $fecha->fecha_inicio . " a " . $fecha->fecha_final;
                    }

                }
                if ($lgtbi_victima != '') {
                    $lgtbi_victima = 'Si';
                } else {
                    $lgtbi_victima = 'No';
                }

                if ($lgtbi_denunciante != '') {
                    $lgtbi_denunciante = 'Si';
                } else {
                    $lgtbi_denunciante = 'No';
                }
                if ($comunidad == '') {
                    $comunidad = 'No aplica';
                }
                if ($ong == '') {
                    $ong = 'No aplica';
                }
                if ($identificacion == '') {
                    $identificacion = 'No aplica';
                }
                if ($nombres == '') {
                    $nombres = 'No aplica';
                }
                if ($apellido == '') {
                    $apellido = 'No aplica';
                }
                if ($genero == '') {
                    $genero = 'No aplica';
                }
                if ($ocupacion_victima == '') {
                    $ocupacion_victima = 'No aplica';
                }

                $array[$key] = [
                    'radicado' => $radicado,
                    'identificacion_vicitima' => $identificacion,
                    'nombres_vicitima' => $nombres,
                    'apellido_victima' => $apellido,
                    'genero_victima' => $genero,
                    'lgtbi_vitima' => $lgtbi_victima,
                    'ocupacion_victima' => $ocupacion_victima,
                    'identificacion_denunciante' => $identificacion_denunciante,
                    'nombres_denunciante' => $nombres_denunciante,
                    'apellido_denunciante' => $apellido_denunciante,
                    'genero_denunciante' => $genero_denunciante,
                    'lgtbi_denunciante' => $lgtbi_denunciante,
                    'ocupacion_denunciante' => $ocupacion_denunciante,
                    'escolaridad_denunciante' => $escolaridad_denunciante,
                    'escolaridad_victima' => $escolaridad_victima,
                    'departamento'=> $departamento,
                    'municipio' => $municipio,
                    'comunidad' => $comunidad,
                    'organizacon' => $ong,
                    'responsable_del_hecho' => $responsable,
                    'hecho_vitimizante' => $hechovictimisante,
                    'periodos' => $periodos

                ];

            }


             // return response()->json($array, 201);
            return Excel::download(new CasosExport($array), 'ReporteCasos.xlsx');

        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }
}

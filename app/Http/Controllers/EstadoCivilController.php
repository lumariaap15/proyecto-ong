<?php

namespace App\Http\Controllers;

use App\Http\Requests\EstadoCivilRequest;
use App\Http\Requests\EstadoCivilRequest as EstadoCivilRequestAlias;
use App\Models\EstadoCivil;
use App\Models\Persona;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class EstadoCivilController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['index','show']]);
    }

    /**
     * @OA\Get(
     *     tags={"estado_civiles"},
     *     path="/api/estado_civiles",
     *     summary="Muestra todos los estados civiles",
     *     @OA\Parameter(
     *          name="page",
     *          description="numero de pagina, enviar solo si se desea paginar, de lo contrario regresa todos los registros",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="termino",
     *          description="descripcion del estado civil",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar todos los estado civiles."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index(){
        try{
            $termino = \request()->has('termino') ? \request()->get('termino') : '';
            if (\request()->has('page')) {
                $estadoCivils = EstadoCivil::query()->where(function ($query) use ($termino){
                    if($termino!=''){
                        $query->where('descripcion','like','%'.$termino.'%');
                    }
                })->paginate(10);
            }else{
                $estadoCivils = EstadoCivil::query()->where(function ($query) use ($termino){
                    if($termino!=''){
                        $query->where('descripcion','like','%'.$termino.'%');
                    }
                })->get();
            }
            return response()->json($estadoCivils,200);
        }catch (\Exception $exception){
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     tags={"estado_civiles"},
     *     path="/api/estado_civiles",
     *     summary="Crea estado civil",
     *     security={{"bearer": {}}},
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="Soltero"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="Estado civil creado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param EstadoCivilRequestAlias $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(EstadoCivilRequestAlias $request){
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $estadoCivil = new EstadoCivil();
                $estadoCivil->fill($request->all());
                $estadoCivil->save();
                return response()->json($estadoCivil, 201);
            } catch (\Exception $exception) {
                return response()->json(['error' => $exception->getMessage()], 500);
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }

    /**
     * @OA\Put(
     *     tags={"estado_civiles"},
     *     path="/api/estado_civiles/{id}",
     *     summary="actualizar un estado civil",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del estado civil a actualizar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="Casado"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Estado civil actualizado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param EstadoCivilRequestAlias $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(EstadoCivilRequest $request, $id){
        $respuesta = null;
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $estadoCivil = EstadoCivil::query()->findOrFail($id);
                $estadoCivil->fill($request->all());
                $estadoCivil->save();
                $respuesta = response()->json($estadoCivil, 200);
            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }

    /**
     * @OA\Get(
     *     tags={"estado_civiles"},
     *     path="/api/estado_civiles/{id}",
     *     summary="Retorna un estado civil especifico",
     *     @OA\Parameter(
     *          name="id",
     *          description="id del estado civil a mostrar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Retonar el tipo de dcumento."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id){
        $respuesta = null;
        try{
            $estadoCivil = EstadoCivil::query()->findOrFail($id);
            $respuesta = response()->json($estadoCivil);
        }catch (ModelNotFoundException $exception){
            $respuesta = response()->json(['error' => $exception->getMessage()],404);
        }catch (\Exception $exception){
            $respuesta = response()->json(['error' => $exception->getMessage()],500);
        }finally{
            return $respuesta;
        }
    }

    /**
     * @OA\Delete(
     *     tags={"estado_civiles"},
     *     path="/api/estado_civiles/{id}",
     *     summary="Elimina un estado civil",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del estado civil a eliminar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Tipo estado civil Eliminado."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id){
        $respuesta = null;
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $estadoCivil = EstadoCivil::query()->findOrFail($id);
                $extisPersona = Persona::query()->where('estado_civil_id', $id)->exists();
                if (!$extisPersona) {
                    $estadoCivil->delete();
                    $respuesta = response()->json($estadoCivil);
                } else {
                    $respuesta = response()->json(['error' => 'No se puede eliminar'], 400);
                }

            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }
}

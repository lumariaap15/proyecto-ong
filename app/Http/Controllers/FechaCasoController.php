<?php

namespace App\Http\Controllers;

use App\Models\Caso;
use App\Models\FechaCaso;
use App\Models\VersionCaso;
use App\User;
use DB;
use Illuminate\Http\Request;

class FechaCasoController extends Controller
{


    public function __construct()
    {
        $this->middleware('jwt.verify');
    }

    /**
     * @OA\Post(
     *     tags={"fecha_caso"},
     *     path="/api/fecha_caso/caso/{caso_id}",
     *     summary="Crea una nueva fecha a un caso",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="caso_id",
     *          description="id al caso que se le inserta la nueva fecha",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="tipo_fecha", type="integer", example="mes"),
     *         @OA\Property(property="fecha_inicio", type="integer", example="2019/11/29"),
     *         @OA\Property(property="fecha_final", type="string", example="2019/12/29"),
     *         @OA\Property(property="comentario", type="string", example="Cometario actualizacion, solo aplica a usuario caso"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="fecha creada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param Request $request
     * @param $caso_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request, $caso_id)
    {
        $existe = FechaCaso::query()
            ->where('tipo_fecha', $request->tipo_fecha)
            ->whereDate('fecha_inicio', $request->fecha_inicio)
            ->whereDate('fecha_final', $request->fecha_final)
            ->where('caso_id', $caso_id)->exists();

        if ($existe) {
            return response()->json(['error' => 'Ya se registro esta fecha para este caso'], 400);
        }
        DB::beginTransaction();
        try {
            $caso = Caso::query()->findOrFail($caso_id);
            if (auth()->user()->hasRole(User::ROLE_ADMIN) || auth()->user()->hasRole(User::ROLE_CASOS)) {
                if (auth()->user()->hasRole(User::ROLE_ADMIN)) {
                    $fecha = new FechaCaso();
                    $fecha->tipo_fecha = $request->tipo_fecha;
                    $fecha->fecha_inicio = $request->fecha_inicio;
                    $fecha->fecha_final = $request->fecha_final;
                    $fecha->caso_id = $caso_id;
                    $fecha->save();
                    DB::commit();
                    return response()->json($fecha, 201);
                } else {
                    $fecha = (object)[];
                    $fecha->tipo_fecha = $request->tipo_fecha;
                    $fecha->fecha_inicio = $request->fecha_inicio;
                    $fecha->fecha_final = $request->fecha_final;
                    $fecha->caso_id = $caso_id;
                    $radicado = $caso->radicado;
                    $version = $this->storeVersion($radicado, 'fecha_casos', null, 'create', null, $fecha, $request->comentario);
                    $this->storeNotificacion($radicado, 'El usuario ' . auth()->user()->name . ' agrego una fecha al caso ' . $radicado, $version->id);
                    DB::commit();
                    return response()->json($fecha, 201);
                }
            } else {
                DB::rollBack();
                return response()->json(['error' => 'This action is unauthorized.'], 403);
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }


    /**
     * @OA\Put(
     *     tags={"fecha_caso"},
     *     path="/api/fecha_caso/fecha/{caso_id}",
     *     summary="edita una  fecha a un caso",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="fecha_id",
     *          description="id de la fecha a editar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="tipo_fecha", type="integer", example="mes"),
     *         @OA\Property(property="fecha_inicio", type="integer", example="2019/11/29"),
     *         @OA\Property(property="fecha_final", type="string", example="2019/12/29"),
     *         @OA\Property(property="comentario", type="string", example="Cometario actualizacion, solo aplica a usuario caso"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="fecha editada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param Request $request
     * @param $fecha_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, $fecha_id)
    {
        DB::beginTransaction();
        try {
            if (auth()->user()->hasRole(User::ROLE_ADMIN) || auth()->user()->hasRole(User::ROLE_CASOS)) {
                $fecha = FechaCaso::with('caso')->findOrFail($fecha_id);
                if (auth()->user()->hasRole(User::ROLE_ADMIN)) {
                    $fecha->tipo_fecha = $request->tipo_fecha;
                    $fecha->fecha_inicio = $request->fecha_inicio;
                    $fecha->fecha_final = $request->fecha_final;
                    $fecha->save();
                } else {
                    $datos_viejos = (object)[];
                    $datos_nuevos = (object)[];
                    $radicado = $fecha->caso->radicado;
                    $existe = VersionCaso::query()
                        ->where('numero_radicado', $radicado)
                        ->where('nombre_tabla', 'fecha_casos')
                        ->where('tabla_id', $fecha->id)
                        //->where('accion', 'update')
                        //->where('user_id', auth()->id())
                        ->where('estado', 'pendiente')
                        ->exists();
                    if ($existe) {
                        return response()->json(['error', 'Tiene una actualizacion pendiente por aprobación'], 400);
                    }

                    if ($request->has('tipo_fecha') && $request->filled('tipo_fecha')) {
                        $datos_nuevos->tipo_fecha = $request->tipo_fecha;
                        $datos_viejos->tipo_fecha = $fecha->tipo_fecha;
                    }
                    if ($request->has('fecha_inicio') && $request->filled('fecha_inicio')) {
                        $datos_nuevos->fecha_inicio = $request->fecha_inicio;
                        $datos_viejos->fecha_inicio = $fecha->fecha_inicio;
                    }

                    if ($request->has('fecha_final') && $request->filled('fecha_final')) {
                        $datos_nuevos->fecha_final = $request->fecha_final;
                        $datos_viejos->fecha_final = $fecha->fecha_final;
                    }

                    $version = $this->storeVersion($radicado, 'fecha_casos', $fecha->id, 'update', $datos_viejos, $datos_nuevos, $request->comentario);
                    $this->storeNotificacion($radicado, 'El usuario ' . auth()->user()->name . ' actualizo una fecha al caso ' . $radicado, $version->id);
                }
                DB::commit();
                return response()->json($fecha);
            } else {
                DB::rollBack();
                return response()->json(['error' => 'This action is unauthorized.'], 403);
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Delete(
     *     tags={"fecha_caso"},
     *     path="/api/fecha_caso/fecha/{fecha_id}",
     *     summary="Elimina un afecha",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="fecha_id",
     *          description="id de la fecha a eliminar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="comentario", type="string", example="Cometario actualizacion, solo aplica a usuario caso"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="fecha eliminada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param Request $request
     * @param $fecha_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $fecha_id)
    {
        DB::beginTransaction();
        try {
            if (auth()->user()->hasRole(User::ROLE_ADMIN) || auth()->user()->hasRole(User::ROLE_CASOS)) {
                $fecha = FechaCaso::with('caso')->findOrFail($fecha_id);
                if (auth()->user()->hasRole(User::ROLE_ADMIN)) {
                    $fecha->delete();
                } else {
                    $radicado = $fecha->caso->radicado;
                    $existe = VersionCaso::query()
                        ->where('numero_radicado', $radicado)
                        ->where('nombre_tabla', 'fecha_casos')
                        ->where('tabla_id', $fecha->id)
                        //->where('accion', 'delete')
                        ->where('estado', 'pendiente')
                        ->exists();
                    if ($existe) {
                        return response()->json(['error' => 'Tiene una actualizacion pendiente por aprobación'], 400);
                    }
                    $version = $this->storeVersion($radicado, 'fecha_casos', $fecha->id, 'delete', $fecha, null, $request->comentario);
                    $this->storeNotificacion($radicado, 'El usuario ' . auth()->user()->name . ' elimino una fecha al caso ' . $radicado, $version->id);
                }
                DB::commit();
                return response()->json($fecha, 200);
            } else {
                DB::rollBack();
                return response()->json(['error' => 'This action is unauthorized.'], 403);
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }
}

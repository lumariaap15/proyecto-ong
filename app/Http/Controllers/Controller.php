<?php

namespace App\Http\Controllers;

use App\Models\Caso;
use App\Models\Notificacion;
use App\Models\VersionCaso;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;


/**
 * @OA\Info(title="API ONG", version="1.0")
 *
 *
 * @OA\SecurityScheme(
 *     type="http",
 *     scheme="bearer",
 *     in="header",
 *     securityScheme="bearer",
 *     )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function storeVersion($radicado, $tabla, $tabla_id, $accion, $valor_antes = null, $valor_despues = null, $comentario = null)
    {
        $version = new VersionCaso();
        $version->numero_radicado = $radicado;
        $version->nombre_tabla = $tabla;
        $version->tabla_id = $tabla_id;
        $version->accion = $accion;
        $version->valor_antes = $valor_antes != null ? json_encode($valor_antes) : "{}";
        $version->valor_despues = $valor_despues != null ? json_encode($valor_despues) : "{}";
        $version->fecha = Carbon::now();
        $version->user_id = auth()->id();
        $version->comentario = $comentario;
        $version->save();
        return $version;
    }

    public function storeNotificacion($radicado, $mensaje, $version_id,$user_target=null)
    {
        $notificacion = new Notificacion();
        $notificacion->fecha = Carbon::now();
        $notificacion->user_id = auth()->id();
        $notificacion->user_target = $user_target;
        $notificacion->radicado = $radicado;
        $notificacion->mensaje = $mensaje;
        $notificacion->version_caso_id = $version_id;
        $caso = Caso::query()->where('radicado',$radicado)->first();
        $notificacion->caso_id = $caso->id;
        $notificacion->save();
        return $notificacion;
    }
}

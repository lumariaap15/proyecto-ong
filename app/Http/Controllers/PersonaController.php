<?php

namespace App\Http\Controllers;

use App\Http\Requests\PersonaRequest;
use App\Models\Caso;
use App\Models\Persona;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class PersonaController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['index', 'show', 'casos']]);
    }


    /**
     * @OA\Get(
     *     tags={"persona"},
     *     path="/api/personas",
     *     summary="Muestra todas las personas",
     *     @OA\Parameter(
     *          name="page",
     *          description="numero de pagina, enviar solo si se desea paginar, de lo contrario regresa todos los registros",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="termino",
     *          description="termino de busqueda en los campos de persona",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar grupos etnicos."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index(Request $request)
    {

        /*$field = 'created_at';
        $dir = 'desc';
        if ($request->has('sort')) {
            if ($request->filled('sort')) {
                $sortRules = $request->input('sort');
                list($field, $dir) = explode('|', $sortRules);
            }
        }*/
        $termino = "";
        if ($request->has('termino')) {
            $termino = $request->input('termino');
        }
        $limit = $request->input('per_page');
        if($request->has('page')){
            $personas = Persona::with(
                'municipio',
                'departamento',
                'tipoDocumento',
                'estadoCivil',
                'gradoEscolaridad',
                'grupoEtnico',
                'grupoIndigena'
            )
                ->buscarCoincidencia($termino)
                ->paginate($limit);
        }else{
            $personas = Persona::with(
                'municipio',
                'departamento',
                'tipoDocumento',
                'estadoCivil',
                'gradoEscolaridad',
                'grupoEtnico',
                'grupoIndigena'
            )
                ->buscarCoincidencia($termino)
                ->get();
        }

        return response()->json($personas, 200);
    }


    /**
     * @OA\Post(
     *     tags={"persona"},
     *     path="/api/personas",
     *     summary="Crea personas",
     *     security={{"bearer": {}}},
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="tipo_documento_id", type="integer", example="1"),
     *         @OA\Property(property="n_documento", type="integer", example="123456789"),
     *         @OA\Property(property="nombres", type="string", example="deiby"),
     *         @OA\Property(property="apellido", type="string", example="tovar"),
     *         @OA\Property(property="fecha_nacimiento", type="string", example="1985-08-18"),
     *         @OA\Property(property="estado_civil_id", type="integer", example="1"),
     *         @OA\Property(property="grado_escolaridad_id", type="integer", example="1"),
     *         @OA\Property(property="genero", type="string", example="masculino"),
     *         @OA\Property(property="municipio_codigo", type="string", example="50001"),
     *         @OA\Property(property="departamento_codigo", type="string", example="50"),
     *         @OA\Property(property="grupo_etnico_id", type="integer", example="1"),
     *         @OA\Property(property="grupo_indigena_id", type="integer", example="1"),
     *         @OA\Property(property="ocupacion", type="string", example="Alguna ocupacion"),
     *         @OA\Property(property="direccion", type="string", example="calle false"),
     *         @OA\Property(property="lgtbi", type="integer", example="0"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="Persona creada creado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param PersonaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PersonaRequest $request)
    {
        if (auth()->user()->hasRole(User::ROLE_ADMIN) || auth()->user()->hasRole(User::ROLE_CASOS)) {
            try {
                $persona = new Persona();
                $persona->fill($request->all());
                $persona->save();
                return response()->json($persona, 201);
            } catch (\Exception $exception) {
                return response()->json(['error' => $exception->getMessage()], 500);
            }
        } else {
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }

    /**
     * @OA\Get(
     *     tags={"persona"},
     *     path="/api/personas/{id}",
     *     summary="Retorna una persona especifico",
     *     @OA\Parameter(
     *          name="id",
     *          description="id de la persona a mostrar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Retonar la persona."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $respuesta = null;
        try {
            $persona = Persona::query()->findOrFail($id);
            $respuesta = response()->json($persona);
        } catch (ModelNotFoundException $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 500);
        } finally {
            return $respuesta;
        }
    }


    /**
     * @OA\Put(
     *     tags={"persona"},
     *     path="/api/personas/{id}",
     *     summary="Crea actualiza persona",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id de la persona a mostrar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="tipo_documento_id", type="integer", example="1"),
     *         @OA\Property(property="n_documento", type="integer", example="123456789"),
     *         @OA\Property(property="nombres", type="string", example="deiby"),
     *         @OA\Property(property="apellido", type="string", example="tovar"),
     *         @OA\Property(property="fecha_nacimiento", type="string", example="1985-08-18"),
     *         @OA\Property(property="estado_civil_id", type="integer", example="1"),
     *         @OA\Property(property="grado_escolaridad_id", type="integer", example="1"),
     *         @OA\Property(property="genero", type="string", example="masculino"),
     *         @OA\Property(property="municipio_codigo", type="string", example="50001"),
     *         @OA\Property(property="departamento_codigo", type="string", example="50"),
     *         @OA\Property(property="grupo_etnico_id", type="integer", example="1"),
     *         @OA\Property(property="grupo_indigena_id", type="integer", example="1"),
     *         @OA\Property(property="ocupacion", type="string", example="Alguna ocupacion"),
     *         @OA\Property(property="direccion", type="string", example="calle false"),
     *         @OA\Property(property="lgtbi", type="integer", example="0"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Persona actualizada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param PersonaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PersonaRequest $request, $id)
    {
        $respuesta = null;
        if (auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $persona = Persona::query()->findOrFail($id);
                $persona->fill($request->all());
                $persona->save();
                $respuesta = response()->json($persona);
            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        } else {
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }


    /**
     * @OA\Delete(
     *     tags={"persona"},
     *     path="/api/personas/{id}",
     *     summary="Elimina una persona especifico",
     *     @OA\Parameter(
     *          name="id",
     *          description="id de la persona a eliminar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="elimina la persona."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $respuesta = null;
        if (auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $persona = Persona::query()->findOrFail($id);
                $existeCaso = Caso::query()->where('victima_id', $id)->orWhere('denunciante_id', $id)->exists();
                if (!$existeCaso) {
                    $persona->delete();
                    $respuesta = response()->json($persona);
                } else {
                    $respuesta = response()->json(['error' => 'No se puede eliminar una persona que este relacionada con un caso'], 400);
                }
            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        } else {
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }


    /**
     * @OA\Get(
     *     tags={"persona"},
     *     path="/api/personas/{id}/casos",
     *     summary="retornas los casos de una persona especifica",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id de la persona a cosnultar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="retorna los casos."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function casos($id)
    {
        try {
            $casos = Caso::query()->where('denunciante_id', $id)->orWhere('victima_id', $id)->get();
            return response()->json($casos);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()]);
        }
    }
}

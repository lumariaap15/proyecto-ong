<?php

namespace App\Http\Controllers;

use App\Http\Requests\GrupoIndigenaRequest;
use App\Models\GrupoIndigena;
use App\Models\Persona;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class GrupoIndigenaController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['index', 'show']]);
    }

    /**
     * @OA\Get(
     *     tags={"grupo_indigena"},
     *     path="/api/grupo_indigenas",
     *     summary="Muestra todos los grupos indigenas",
     *     @OA\Parameter(
     *          name="page",
     *          description="numero de pagina, enviar solo si se desea paginar, de lo contrario regresa todos los registros",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="termino",
     *          description="descripcion del grupo etnico",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar grupos etnicos."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index()
    {
        try {
            $termino = \request()->has('termino') ? \request()->get('termino') : '';
            if (\request()->has('page')) {
                $gropoIndigenas = GrupoIndigena::query()->where(function ($query) use ($termino) {
                    if ($termino != '') {
                        $query->where('descripcion', 'like', '%' . $termino . '%');
                    }
                })->paginate(10);
            } else {
                $gropoIndigenas = GrupoIndigena::query()->where(function ($query) use ($termino) {
                    if ($termino != '') {
                        $query->where('descripcion', 'like', '%' . $termino . '%');
                    }
                })->get();
            }
            return response()->json($gropoIndigenas, 200);
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     tags={"grupo_indigena"},
     *     path="/api/grupo_indigenas",
     *     summary="Crea un grupo indigena",
     *     security={{"bearer": {}}},
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="Guayu"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="grupo indigena creada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param GrupoIndigenaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(GrupoIndigenaRequest $request)
    {
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $gropoIndigena = new GrupoIndigena();
                $gropoIndigena->fill($request->all());
                $gropoIndigena->save();
                return response()->json($gropoIndigena, 201);
            } catch (\Exception $exception) {
                return response()->json(['error' => $exception->getMessage()], 500);
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }

    /**
     * @OA\Put(
     *     tags={"grupo_indigena"},
     *     path="/api/grupo_indigenas/{id}",
     *     summary="actualizar un grupo indigena",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del grupo indigena a actualizar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="Guayu 2"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="grupo indigena actualizado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param GrupoIndigenaRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(GrupoIndigenaRequest $request, $id)
    {
        $respuesta = null;
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $gropoIndigena = GrupoIndigena::query()->findOrFail($id);
                $gropoIndigena->fill($request->all());
                $gropoIndigena->save();
                $respuesta = response()->json($gropoIndigena, 200);
            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }

    /**
     * @OA\Get(
     *     tags={"grupo_indigena"},
     *     path="/api/grupo_indigenas/{id}",
     *     summary="Retorna un grupo indigena especifico",
     *     @OA\Parameter(
     *          name="id",
     *          description="id del grupo indigena a mostrar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Retonar el grupo indigena."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $respuesta = null;
        try {
            $gropoIndigena = GrupoIndigena::query()->findOrFail($id);
            $respuesta = response()->json($gropoIndigena);
        } catch (ModelNotFoundException $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 500);
        } finally {
            return $respuesta;
        }
    }

    /**
     * @OA\Delete(
     *     tags={"grupo_indigena"},
     *     path="/api/grupo_indigenas/{id}",
     *     summary="Elimina un grupo indigena",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del grupo indigena a eliminar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="grupo indigena Eliminado."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $respuesta = null;
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $gropoIndigena = GrupoIndigena::query()->findOrFail($id);
                $extisPersona = Persona::query()->where('grupo_indigena_id', $id)->exists();
                if (!$extisPersona) {
                    $gropoIndigena->delete();
                    $respuesta = response()->json($gropoIndigena);
                } else {
                    $respuesta = response()->json(['error' => 'No se puede eliminar'], 400);
                }
            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }

}

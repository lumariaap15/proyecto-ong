<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Mockery\Exception;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify');
    }


    /**
     * @OA\Get(
     *     tags={"usuario"},
     *     path="/api/usuarios",
     *     summary="Muestra todas los usuarios",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="page",
     *          description="numero de pagina, enviar solo si se desea paginar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="termino",
     *          description="termino de busqueda en los campos del usuario",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar todos los usuarios."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index(Request $request)
    {
        $this->authorize('view', new User());
        $termino = "";
        if ($request->has('termino')) {
            $termino = $request->input('termino');
        }
        $limit = $request->input('per_page');
        $usuarios = User::query()
            ->buscarCoincidencia($termino)
            ->paginate($limit);
        return response()->json($usuarios, 200);
    }


    /**
     * @OA\Post(
     *     tags={"usuario"},
     *     path="/api/usuarios",
     *     summary="Crea usuario",
     *     security={{"bearer": {}}},
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="name", type="string", example="Pepito Perez"),
     *         @OA\Property(property="email", type="string", example="pepito@gmail.com"),
     *         @OA\Property(property="role", type="string", example="administrador,consulta,casos"),
     *         @OA\Property(property="password", type="string", example="password"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="usuario creado creado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param UserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UserRequest $request)
    {
        try {
            $user = new User();
            $this->authorize('create', $user);
            $user->fill($request->all());
            $user->password = bcrypt($request->password);
            $user->save();
            return response()->json($user, 201);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
    }

    /**
     * @OA\Get(
     *     tags={"usuario"},
     *     path="/api/usuarios/{id}",
     *     summary="Retorna un usuario especifico",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del usuario a mostrar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Retonar el usuario."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $respuesta = null;
        try {
            $user = User::query()->findOrFail($id);
            $this->authorize('view', $user);
            $respuesta = response()->json($user);
        } catch (ModelNotFoundException $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 500);
        } finally {
            return $respuesta;
        }
    }


    /**
     * @OA\Put(
     *     tags={"usuario"},
     *     path="/api/usuarios/{id}",
     *     summary="actualiza usuario",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del usuario a actualizar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *          @OA\Property(property="name", type="string", example="Pepito Perez"),
     *         @OA\Property(property="email", type="string", example="pepito@gmail.com"),
     *         @OA\Property(property="role", type="string", example="administrador,consulta,casos"),
     *         @OA\Property(property="password", type="string", example="password"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Usuario actualizada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UserRequest $request, $id)
    {
        $respuesta = null;
        try {
            $user = User::query()->findOrFail($id);
            $this->authorize('update', $user);
            if($request->has('email') && $request->filled('email')){
                $user->email = $request->email;
            }
            if($request->has('name') && $request->filled('name')){
                $user->name = $request->name;
            }
            if($request->has('role') && $request->filled('role')){
                $user->role = $request->role;
            }
            if($request->has('password') && $request->filled('password')){
                $user->password = bcrypt($request->password);
            }
            if($user->isDirty()){
                $user->save();
            }
            $respuesta = response()->json($user);
        } catch (ModelNotFoundException $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 500);
        } finally {
            return $respuesta;
        }
    }


    /**
     * @OA\Delete(
     *     tags={"usuario"},
     *     path="/api/usuarios/{id}",
     *     summary="Elimina un usuario especifico",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id de la persona a eliminar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="elimina el usuario."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $respuesta = null;
        try {
            if(auth()->id() != $id){
                $user = User::query()->findOrFail($id);
                $this->authorize('delete', $user);

                $user->delete();
                $respuesta = response()->json($user);
            }else{
                $respuesta = response()->json(['error' => 'No se puede eliminar a usted mismo'],400);
            }
        } catch (ModelNotFoundException $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 500);
        } finally {
            return $respuesta;
        }
    }


    /**
     * @OA\Put(
     *     tags={"usuario"},
     *     path="/api/usuarios/password",
     *     summary="actualiza contraseña usuario autenticado",
     *     security={{"bearer": {}}},
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *          @OA\Property(property="current_password", type="string", example="password"),
     *         @OA\Property(property="password", type="string", example="password2"),
     *         @OA\Property(property="password_confirmation", type="string", example="password2"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Usuario password actualizada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request)
    {
        $respuesta = null;
        try{
            $validation = \Validator::make($request->all(),[
                'password' => 'required|min:6|max:12|confirmed',
                'password_confirmation' => 'required',
                'current_password' => 'required|min:6|max:12'
            ]);

            if($validation->fails()){
                $respuesta = response()->json(['error'=> $validation->errors()->all()[0]],422);
            }else{
                $user = auth()->user();
                if(\Hash::check($request->current_password, $user->password)){
                    $user->password = bcrypt($request->password);
                    $user->save();
                    $respuesta = response()->json($user);
                }else{
                    $respuesta = response()->json(['error'=> 'La contraseña ingresa no es la actual contraseña'],400);
                }
            }
        }catch (ModelNotFoundException $exception){
            $respuesta = response()->json(['error'=> $exception->getMessage()],404);
        }catch (\Exception $exception){
            $respuesta = response()->json(['error'=> $exception->getMessage()],500);
        }finally{
            return $respuesta;
        }
    }
}

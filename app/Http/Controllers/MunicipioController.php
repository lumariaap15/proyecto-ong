<?php

namespace App\Http\Controllers;

use App\Models\Municipio;
use Illuminate\Http\Request;

class MunicipioController extends Controller
{

    /**
     * @OA\Get(
     *     tags={"municipios"},
     *     path="/api/municipios/{codigo}",
     *     summary="Muestra todos los municipios del departamento",
     *     @OA\Parameter(
     *          name="codigo",
     *          description="codigo del deparrtamento, del que se quiere obtener sus municipios",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar todos los municipios."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index($codigo){
        $municipios = Municipio::query()->where('departamento_codigo', $codigo)
            ->orderBy('descripcion','asc')->get();

        return response()->json($municipios);
    }
}

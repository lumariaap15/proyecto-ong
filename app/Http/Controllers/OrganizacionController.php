<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrganizacionRequest;
use App\Models\Caso;
use App\Models\Organizacion;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class OrganizacionController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['index', 'show']]);
    }
    /**
     * @OA\Get(
     *     tags={"organización"},
     *     path="/api/organizaciones",
     *     summary="Muestra todos las organizaciones",
     *     @OA\Parameter(
     *          name="page",
     *          description="numero de pagina, enviar solo si se desea paginar, de lo contrario regresa todos los registros",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="termino",
     *          description="descripción de la organización",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar todas las organizaciones."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index()
    {
        try {
            $termino = \request()->has('termino') ? \request()->get('termino') : '';
            if (\request()->has('page')) {
                $organizacion = Organizacion::query()->where(function ($query) use ($termino) {
                    if ($termino != '') {
                        $query->where('descripcion', 'like', '%' . $termino . '%');
                    }
                })->paginate(10);
            } else {
                $organizacion = Organizacion::query()->where(function ($query) use ($termino) {
                    if ($termino != '') {
                        $query->where('descripcion', 'like', '%' . $termino . '%');
                    }
                })->get();
            }
            return response()->json($organizacion, 200);
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     tags={"organización"},
     *     path="/api/organizaciones",
     *     summary="Crear una organizacion",
     *     security={{"bearer": {}}},
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="Indigena"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="Organización creada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param OrganizacionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(OrganizacionRequest $request)
    {
        try {
            $organizacion = new Organizacion();
            $organizacion->fill($request->all());
            $organizacion->save();
            return response()->json($organizacion, 201);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
    }
    /**
     * @OA\Put(
     *     tags={"organización"},
     *     path="/api/organizaciones/{id}",
     *     summary="actualizar un organizacion",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id de la organización a actualizar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="Comunidad"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Organización actualizada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param OrganizacionRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(OrganizacionRequest $request, $id)
    {
        $respuesta = null;
        try {
            $organizacion = Organizacion::query()->findOrFail($id);
            $organizacion->fill($request->all());
            $organizacion->save();
            $respuesta = response()->json($organizacion, 200);
        } catch (ModelNotFoundException $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 500);
        } finally {
            return $respuesta;
        }
    }


    /**
     * @OA\Get(
     *     tags={"organización"},
     *     path="/api/organizaciones/{id}",
     *     summary="Retorna una organizacion especifica",
     *     @OA\Parameter(
     *          name="id",
     *          description="id de la organización a mostrar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Retonar la organización."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $respuesta = null;
        try {
            $organizacion = Organizacion::query()->findOrFail($id);
            $respuesta = response()->json($organizacion);
        } catch (ModelNotFoundException $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 500);
        } finally {
            return $respuesta;
        }
    }

    /**
     * @OA\Delete(
     *     tags={"organización"},
     *     path="/api/organizaciones/{id}",
     *     summary="Elimina una organizaión",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id de la organiización a eliminar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Organización Eliminada."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function destroy($id)
    {
        $respuesta = null;
        try {
            $organizacion = Organizacion::query()->findOrFail($id);
            $existeOrganizacion = Caso::query()->where('organizacion_victima_id',$id)->exists();
            if (!$existeOrganizacion) {
                $organizacion->delete();
                $respuesta = response()->json($organizacion);
            } else {
                $respuesta = response()->json(['error' => 'No se puede eliminar una organizacion que este siendo usada'], 400);
            }
        } catch (ModelNotFoundException $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 500);
        } finally {
            return $respuesta;
        }

    }
}

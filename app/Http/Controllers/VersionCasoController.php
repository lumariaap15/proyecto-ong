<?php

namespace App\Http\Controllers;

use App\Models\Caso;
use App\Models\DocumentoCasos;
use App\Models\FechaCaso;
use App\Models\ObservacionCaso;
use App\Models\VersionCaso;
use App\User;
use DB;
use Illuminate\Http\Request;
use Storage;

class VersionCasoController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.verify');
    }

    /**
     * @OA\Get(
     *     tags={"version_caso"},
     *     path="/api/versioncasos",
     *     summary="Muestra todas las solicitudes de cambios a un caso",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="estado",
     *          description="el estado en el que esta dicho cambio en el caso, 'aprobado','cancelado','pendiente'",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="page",
     *          description="numero de pagina, enviar solo si se desea paginar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="radicado",
     *          description="numero de radicado del caso",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar los casos."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index(Request $request)
    {
        if (auth()->user()->hasRole(User::ROLE_ADMIN)) {
            $estado = $request->has('estado') ? $request->get('estado') : '';
            $radicado = $request->has('radicado') ? $request->get('radicado') : '';
            $versionCaso = VersionCaso::with('user')
                ->where(function ($query) use ($estado) {
                    if ($estado != '') {
                        $query->where('estado', $estado);
                    }
                })
                ->where(function ($query) use ($radicado) {
                    if ($radicado != '') {
                        $query->where('numero_radicado', $radicado);
                    }
                })
                ->latest()
                ->paginate(10);
            return response()->json($versionCaso, 200);
        } else {
            return response()->json(['error' => 'This action is unauthorized.'], 403);
        }
    }


    /**
     * @OA\Get(
     *     tags={"version_caso"},
     *     path="/api/versioncasos/user/mis_versiones",
     *     summary="Muestra todas las solicitudes de cambios del usuario autenticado",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="estado",
     *          description="el estado en el que esta dicho cambio en el caso, 'aprobado','cancelado','pendiente'",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="page",
     *          description="numero de pagina, enviar solo si se desea paginar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="radicado",
     *          description="numero de radicado del caso",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar los casos."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function misVersiones(Request $request)
    {
        $estado = $request->has('estado') ? $request->get('estado') : '';
        $radicado = $request->has('radicado') ? $request->get('radicado') : '';
        $versionCaso = VersionCaso::with('user')
            ->where(function ($query) use ($estado) {
                if ($estado != '') {
                    $query->where('estado', $estado);
                }
            })
            ->where(function ($query) use ($radicado) {
                if ($radicado != '') {
                    $query->where('numero_radicado', $radicado);
                }
            })
            ->where('user_id',auth()->id())
            ->latest()
            ->paginate();
        return response()->json($versionCaso, 200);
    }


    /**
     * @OA\Put(
     *     tags={"version_caso"},
     *     path="/api/versioncasos/{id}",
     *     summary="Permite aceptar o rechazar un versionamiento de caso",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id de la version que se desea gestionar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="estado", type="string", example="aprobado,cancelado,pendiente"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Version de caso modificada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirmarVersion(Request $request, $id)
    {
        $resultado = null;
        try {
            if (auth()->user()->hasRole(User::ROLE_ADMIN)) {
                DB::beginTransaction();
                $version = VersionCaso::query()->findOrFail($id);
                if ($request->estado == 'aprobado' && $version->estado == 'pendiente') {
                    $model = $this->retornaModelo($version->accion, $version->nombre_tabla, $version->tabla_id);
                    if ($version->accion == 'delete') {
                        $model->delete();
                    } elseif ($version->accion == 'create') {
                        foreach ($version->valor_despues as $key => $valor) {
                            $model[$key] = $valor;
                        }
                        $model->save();
                    } elseif ($version->accion == 'update') {
                        foreach ($version->valor_despues as $key => $valor) {
                            if ($version->nombre_tabla == 'documento_casos' && $key == 'archivo') {
                                $pathanterior = str_replace('/storage', '', $version->valor_antes[$key]);
                                if (Storage::disk('public')->exists($pathanterior)) {
                                    (Storage::disk('public')->delete($pathanterior));
                                }
                            }
                            if (is_array($valor)) {
                                $model[$key] = $valor[$key];
                            } else {
                                $model[$key] = $valor;
                            }
                        }
                        $model->save();
                    }
                    $version->estado = 'aprobado';
                    $version->save();
                    $this->storeNotificacion($version->numero_radicado, 'El administrador ' . auth()->user()->name . ' aprobó tu solicitud', $version->id, $version->user_id);
                    DB::commit();
                    $resultado = response()->json('caso actualizado', 200);
                } elseif ($request->estado == 'cancelado' && $version->estado == 'pendiente') {
                    $version->estado = 'cancelado';
                    $version->save();
                    $this->storeNotificacion($version->numero_radicado, 'El administrador ' . auth()->user()->name . ' canceló tu solicitud', $version->id, $version->user_id);
                    DB::commit();
                    $resultado = response()->json('caso actualizado', 200);
                } else {
                    $resultado = response()->json(['error' => 'No puede cambiar un de estado una versionq ue ya este rechazada o aceptada'], 400);
                }
            } else {
                $resultado = response()->json(['error' => 'This action is unauthorized.'], 401);
            }
        } catch (\Exception $exception) {
            $resultado = response()->json([
                'error' => $exception->getMessage()
            ], 500);
        } finally {
            return $resultado;
        }
    }


    /**
     * @OA\Get(
     *     tags={"version_caso"},
     *     path="/api/versioncasos/{id}",
     *     summary="Muestra la data de una solicitud de version",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="El id de la version que se quiere visualizar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar los datos de la version."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function show($id)
    {
        try {
            $version = VersionCaso::with('user')->findOrFail($id);
            return response()->json($version);
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }


    private function retornaModelo($accion, $tabla, $tabla_id = null)
    {
        $model = null;
        if ($accion == 'delete' || $accion == 'update') {
            switch ($tabla) {
                case 'documento_casos':
                    $model = DocumentoCasos::query()->findOrFail($tabla_id);
                    break;
                case 'casos':
                    $model = Caso::query()->findOrFail($tabla_id);
                    break;
                case 'observacion_casos':
                    $model = ObservacionCaso::query()->findOrFail(($tabla_id));
                    break;
                case 'fecha_casos':
                    $model = FechaCaso::query()->findOrFail($tabla_id);
                    break;
            }
        } elseif ($accion = 'create') {
            switch ($tabla) {
                case 'documento_casos':
                    $model = new DocumentoCasos();
                    break;
                case 'casos':
                    $model = new Caso();
                    break;
                case 'observacion_casos':
                    $model = new ObservacionCaso();
                    break;
                case 'fecha_casos':
                    $model = new FechaCaso();
                    break;
            }
        }
        return $model;
    }
}

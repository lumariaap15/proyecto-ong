<?php

namespace App\Http\Controllers;

use App\Http\Requests\EstadoProcesoCasoRequest;
use App\Models\Caso;
use App\Models\EstadoProcesoCaso;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class EstadoProcesoCasoController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['index','show']]);
    }

    /**
     * @OA\Get(
     *     tags={"estado_procesos"},
     *     path="/api/estado_procesos",
     *     summary="Muestra todos los estados de procesos de un caso",
     *     @OA\Parameter(
     *          name="page",
     *          description="numero de pagina, enviar solo si se desea paginar, de lo contrario regresa todos los registros",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="termino",
     *          description="descripcion del estado del proceso",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar todos los estados del proceso."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index(){
        try{
            $termino = \request()->has('termino') ? \request()->get('termino') : '';
            if (\request()->has('page')) {
                $estadoCivils = EstadoProcesoCaso::query()->where(function ($query) use ($termino){
                    if($termino!=''){
                        $query->where('descripcion','like','%'.$termino.'%');
                    }
                })->paginate(10);
            }else{
                $estadoCivils = EstadoProcesoCaso::query()->where(function ($query) use ($termino){
                    if($termino!=''){
                        $query->where('descripcion','like','%'.$termino.'%');
                    }
                })->get();
            }
            return response()->json($estadoCivils,200);
        }catch (\Exception $exception){
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     tags={"estado_procesos"},
     *     path="/api/estado_procesos",
     *     summary="Crea estado de proceso a los casos",
     *     security={{"bearer": {}}},
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="Terminado"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="Estado proceso creado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     *
     * @param EstadoProcesoCasoRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(EstadoProcesoCasoRequest $request){
        if(auth()->user()->hasRole(User::ROLE_ADMIN) || auth()->user()->hasRole(User::ROLE_CASOS)) {
            try {
                $estadoProceso = new EstadoProcesoCaso();
                $estadoProceso->fill($request->all());
                $estadoProceso->save();
                return response()->json($estadoProceso, 201);
            } catch (\Exception $exception) {
                return response()->json(['error' => $exception->getMessage()], 500);
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 403);
        }
    }

    /**
     * @OA\Put(
     *     tags={"estado_procesos"},
     *     path="/api/estado_procesos/{id}",
     *     summary="actualizar un estado de proceso",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del estado de proceso a actualizar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="En juicio"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Estado proceso actualizado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param EstadoProcesoCasoRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(EstadoProcesoCasoRequest $request, $id){
        $respuesta = null;
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $estadoProceso = EstadoProcesoCaso::query()->findOrFail($id);
                $estadoProceso->fill($request->all());
                $estadoProceso->save();
                $respuesta = response()->json($estadoProceso, 200);
            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 403);
        }
    }

    /**
     * @OA\Get(
     *     tags={"estado_procesos"},
     *     path="/api/estado_procesos/{id}",
     *     summary="Retorna un estado de proceso especifico",
     *     @OA\Parameter(
     *          name="id",
     *          description="id del estado de proceso a mostrar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Retonar el estado de proceso."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id){
        $respuesta = null;
        try{
            $estadoProceso = EstadoProcesoCaso::query()->findOrFail($id);
            $respuesta = response()->json($estadoProceso);
        }catch (ModelNotFoundException $exception){
            $respuesta = response()->json(['error' => $exception->getMessage()],404);
        }catch (\Exception $exception){
            $respuesta = response()->json(['error' => $exception->getMessage()],500);
        }finally{
            return $respuesta;
        }
    }

    /**
     * @OA\Delete(
     *     tags={"estado_procesos"},
     *     path="/api/estado_procesos/{id}",
     *     summary="Elimina un estado proceso",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del estado de proceso a eliminar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Estado de proceso caso Eliminado."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id){
        $respuesta = null;
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $estadoProceso = EstadoProcesoCaso::query()->findOrFail($id);
                $extisProceso= Caso::query()->where('estado_proceso_caso_id', $id)->exists();
                if (!$extisProceso) {
                    $estadoProceso->delete();
                    $respuesta = response()->json($estadoProceso);
                } else {
                    $respuesta = response()->json(['error' => 'No se puede eliminar'], 400);
                }

            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 403);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\ComunidadRequest;
use App\Models\Caso;
use App\Models\Comunidad;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ComunidadController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['index']]);
    }

    /**
     * @OA\Get(
     *     tags={"comunidades"},
     *     path="/api/comunidad",
     *     summary="Muestra todas las comunidades",
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar todos los estado civiles."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index()
    {
        try {
            $comunidad = Comunidad::with('municipios')->get();
            return response()->json($comunidad, 200);
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     tags={"comunidades"},
     *     path="/api/comunidad",
     *     summary="Crea una comunidad",
     *     security={{"bearer": {}}},
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="Campesina"),
     *         @OA\Property(property="municipio_codigo", type="integer", example="11001"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="Comunidad creada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ComunidadRequest $request)
    {
        if (auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $comunidad = new Comunidad();
                $comunidad->fill($request->all());
                $comunidad->save();
                $comunidad = Comunidad::with('municipios')->findOrFail($comunidad->id);
                return response()->json($comunidad, 201);

            } catch (\Exception $exception) {
                return response()->json(
                    ['error' => $exception->getMessage(), 500]
                );
            }
        } else {
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }

    }

    /**
     * @OA\Put(
     *     tags={"comunidades"},
     *     path="/api/comunidad/{id}",
     *     summary="actualizar una comunidad",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id de la comunidad a actualizar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="Indigena 2"),
     *         @OA\Property(property="municipio_codigo", type="integer", example="13052"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Comunidad actualizada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ComunidadRequest $request, $id)
    {
        if (auth()->user()->HasRole(User::ROLE_ADMIN)) {
            try {
                $comunidad = Comunidad::query()->with('municipios')->findOrFail($id);
                $comunidad->fill($request->all());
                $comunidad->save();
                $comunidad = Comunidad::query()->with('municipios')->findOrFail($id);
                return response()->json($comunidad, 201);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                return response()->json(['error' => $exception->getMessage()], 500);
            }
        } else {
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }


    /**
     * @OA\Delete(
     *     tags={"comunidades"},
     *     path="/api/comunidad/{id}",
     *     summary="Elimina una comunidad",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id de la comunidad eliminar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Comunidad Eliminada."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy($id)
    {

        if (auth()->user()->HasRole(User::ROLE_ADMIN)) {
            $respuesta = null;
            try {
                $comunidad = Comunidad::query()->findOrFail($id);
                $extisCaso = Caso::query()->where('comunidad_id', $id)->exists();
                if (!$extisCaso) {
                    $comunidad->delete();
                    $respuesta = response()->json($comunidad);
                } else {
                    $respuesta = response()->json(['error' => 'No se puede eliminar una comunidad que este en uso'], 400);
                }
            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        } else {
            return response()->json(['error' => 'This action is unauthorized.'], 401);
       }
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\GrupoEtnicoRequest;
use App\Models\GrupoEtnico;
use App\Models\Persona;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class GrupoEtnicoController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['index', 'show']]);
    }

    /**
     * @OA\Get(
     *     tags={"grupo_etnico"},
     *     path="/api/grupo_etnicos",
     *     summary="Muestra todos los grupos etnicos",
     *     @OA\Parameter(
     *          name="page",
     *          description="numero de pagina, enviar solo si se desea paginar, de lo contrario regresa todos los registros",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="termino",
     *          description="descripcion del grupo etnico",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar grupos etnicos."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index()
    {
        try {
            $termino = \request()->has('termino') ? \request()->get('termino') : '';
            if (\request()->has('page')) {
                $grupoEtnicos = GrupoEtnico::query()->where(function ($query) use ($termino) {
                    if ($termino != '') {
                        $query->where('descripcion', 'like', '%' . $termino . '%');
                    }
                })->paginate(10);
            } else {
                $grupoEtnicos = GrupoEtnico::query()->where(function ($query) use ($termino) {
                    if ($termino != '') {
                        $query->where('descripcion', 'like', '%' . $termino . '%');
                    }
                })->get();
            }
            return response()->json($grupoEtnicos, 200);
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     tags={"grupo_etnico"},
     *     path="/api/grupo_etnicos",
     *     summary="Crea un grupo etnico",
     *     security={{"bearer": {}}},
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="Indigena"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="grupo etnico creada."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(GrupoEtnicoRequest $request)
    {
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $grupoEtnico = new GrupoEtnico();
                $grupoEtnico->fill($request->all());
                $grupoEtnico->save();
                return response()->json($grupoEtnico, 201);
            } catch (\Exception $exception) {
                return response()->json(['error' => $exception->getMessage()], 500);
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }

    /**
     * @OA\Put(
     *     tags={"grupo_etnico"},
     *     path="/api/grupo_etnicos/{id}",
     *     summary="actualizar un grupo etnico",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del grupo etnico a actualizar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="Indigena 2"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="grupo etnico actualizado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(GrupoEtnicoRequest $request, $id)
    {
        $respuesta = null;
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $grupoEtnico = GrupoEtnico::query()->findOrFail($id);
                $grupoEtnico->fill($request->all());
                $grupoEtnico->save();
                $respuesta = response()->json($grupoEtnico, 200);
            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }

    /**
     * @OA\Get(
     *     tags={"grupo_etnico"},
     *     path="/api/grupo_etnicos/{id}",
     *     summary="Retorna un grupo etnico especifico",
     *     @OA\Parameter(
     *          name="id",
     *          description="id del grupo etnico a mostrar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Retonar el grupo etnico."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $respuesta = null;
        try {
            $grupoEtnico = GrupoEtnico::query()->findOrFail($id);
            $respuesta = response()->json($grupoEtnico);
        } catch (ModelNotFoundException $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 500);
        } finally {
            return $respuesta;
        }
    }

    /**
     * @OA\Delete(
     *     tags={"grupo_etnico"},
     *     path="/api/grupo_etnicos/{id}",
     *     summary="Elimina un grupo etnico",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del grupo etnico a eliminar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="grupo etnico Eliminado."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $respuesta = null;
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $grupoEtnico = GrupoEtnico::query()->findOrFail($id);
                $extisPersona = Persona::query()->where('grupo_etnico_id', $id)->exists();
                if (!$extisPersona) {
                    $grupoEtnico->delete();
                    $respuesta = response()->json($grupoEtnico);
                } else {
                    $respuesta = response()->json(['error' => 'No se puede eliminar'], 400);
                }

            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }

    }
}

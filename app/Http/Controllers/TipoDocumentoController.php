<?php

namespace App\Http\Controllers;


use App\Http\Requests\TipoDocumentoRequest;
use App\Models\Persona;
use App\Models\TipoDocumento;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class TipoDocumentoController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['index','show']]);
    }

    /**
     * @OA\Get(
     *     tags={"tipo_documentos"},
     *     path="/api/tipo_documentos",
     *     summary="Muestra todos los tipos de documentos",
     *     @OA\Parameter(
     *          name="page",
     *          description="numero de pagina, enviar solo si se desea paginar, de lo contrario regresa todos los registros",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="termino",
     *          description="nombre o sigla del tipo de documento",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar todos los tipos de documentos."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index(){
        try{
            $termino = \request()->has('termino') ? \request()->get('termino') : '';
            if (\request()->has('page')) {
                $tipoDocumentos = TipoDocumento::query()->where(function ($query) use ($termino){
                    if($termino!=''){
                        $query->where('descripcion','like','%'.$termino.'%');
                        $query->orWhere('sigla','like','%'.$termino.'%');
                    }
                })->paginate(10);
            }else{
                $tipoDocumentos = TipoDocumento::query()->where(function ($query) use ($termino){
                    if($termino!=''){
                        $query->where('descripcion','like','%'.$termino.'%');
                        $query->orWhere('sigla','like','%'.$termino.'%');
                    }
                })->get();
            }
            return response()->json($tipoDocumentos,200);
        }catch (\Exception $exception){
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     tags={"tipo_documentos"},
     *     path="/api/tipo_documentos",
     *     summary="Crea un tipo de documento",
     *     security={{"bearer": {}}},
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="sigla", type="string", example="CC"),
     *         @OA\Property(property="descripcion", type="string", example="cedula de ciudadania"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="Tipo documento creado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param TipoDocumentoRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TipoDocumentoRequest $request){
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $tipoDocumento = new TipoDocumento();
                $tipoDocumento->fill($request->all());
                $tipoDocumento->save();
                return response()->json($tipoDocumento, 201);
            } catch (\Exception $exception) {
                return response()->json(['error' => $exception->getMessage()], 500);
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }

    /**
     * @OA\Put(
     *     tags={"tipo_documentos"},
     *     path="/api/tipo_documentos/{id}",
     *     summary="actualizar tipo de documento",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del tipo de documento a actualizar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="sigla", type="string", example="CC"),
     *         @OA\Property(property="descripcion", type="string", example="cedula de ciudadania"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Tipo documento actualizado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param TipoDocumentoRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(TipoDocumentoRequest $request, $id){
        $respuesta = null;
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $tipoDocumento = TipoDocumento::query()->findOrFail($id);
                $tipoDocumento->fill($request->all());
                $tipoDocumento->save();
                $respuesta = response()->json($tipoDocumento, 200);
            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }

    /**
     * @OA\Get(
     *     tags={"tipo_documentos"},
     *     path="/api/tipo_documentos/{id}",
     *     summary="Retorna un tipo de documento especifico",
     *     @OA\Parameter(
     *          name="id",
     *          description="id del tipo de documento a mostrar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Retonar el tipo de dcumento."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param TipoDocumentoRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id){
        $respuesta = null;
        try{
            $tipoDocumento = TipoDocumento::query()->findOrFail($id);
            $respuesta = response()->json($tipoDocumento);
        }catch (ModelNotFoundException $exception){
            $respuesta = response()->json(['error' => $exception->getMessage()],404);
        }catch (\Exception $exception){
            $respuesta = response()->json(['error' => $exception->getMessage()],500);
        }finally{
            return $respuesta;
        }
    }

    /**
     * @OA\Delete(
     *     tags={"tipo_documentos"},
     *     path="/api/tipo_documentos/{id}",
     *     summary="Elimina un tipo de documento",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del tipo de documento a actualizar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Tipo de documento Eliminado."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param TipoDocumentoRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id){
        $respuesta = null;
       if(auth()->user()->hasRole(User::ROLE_ADMIN)){
           try{
               $tipoDocumento = TipoDocumento::query()->findOrFail($id);
               $extisPersona = Persona::query()->where('tipo_documento_id', $id)->exists();
               if(!$extisPersona){
                   $tipoDocumento->delete();
                   $respuesta = response()->json($tipoDocumento);
               }else{
                   $respuesta = response()->json(['error' => 'No se puede eliminar'], 400);
               }
           }catch (ModelNotFoundException $exception){
               $respuesta = response()->json(['error' => $exception->getMessage()],404);
           }catch (\Exception $exception){
               $respuesta = response()->json(['error' => $exception->getMessage()],500);
           }finally{
               return $respuesta;
           }
       }else{
           return response()->json(['error' => 'This action is unauthorized.'], 401);
       }
    }
}

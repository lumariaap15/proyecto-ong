<?php

namespace App\Http\Controllers;

use App\Models\Departamento;
use Illuminate\Http\Request;

class DepartamentoController extends Controller
{
    /**
     * @OA\Get(
     *     tags={"departamentos"},
     *     path="/api/departamentos",
     *     summary="Muestra todos departamentos",
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar todos los departamentos."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index(){
        $departamentos = Departamento::query()->orderBy('descripcion','asc')->get();
        return response()->json($departamentos);
    }
}

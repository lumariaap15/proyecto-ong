<?php

namespace App\Http\Controllers;

use App\Http\Requests\HechoVictimizanteRequest;
use App\Models\Caso;
use App\Models\HechoVictimizante;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class HechoVictimizanteController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['index', 'show']]);
    }

    /**
     * @OA\Get(
     *     tags={"hecho_victimizante"},
     *     path="/api/hechos_victimizantes",
     *     summary="Muestra todos los hechos victimizantes",
     *     @OA\Parameter(
     *          name="page",
     *          description="numero de pagina, enviar solo si se desea paginar, de lo contrario regresa todos los registros",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="termino",
     *          description="descripcion del hecho victimizante",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar todos los hechos victimizantes."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */

    public function index()
    {
        try {
            $termino = \request()->has('termino') ? \request()->get('termino') : '';
            if (\request()->has('page')) {
                $hechoVictimizante = HechoVictimizante::query()->where(function ($query) use ($termino) {
                    if ($termino != '') {
                        $query->where('descripcion', 'like', '%' . $termino . '%');
                    }
                })->paginate(10);
            } else {
                $hechoVictimizante = HechoVictimizante::query()->where(function ($query) use ($termino) {
                    if ($termino != '') {
                        $query->where('descripcion', 'like', '%' . $termino . '%');
                    }
                })->get();
            }
            return response()->json($hechoVictimizante, 200);
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     tags={"hecho_victimizante"},
     *     path="/api/hechos_victimizantes",
     *     summary="Crea un hecho victimizante",
     *     security={{"bearer": {}}},
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="Secuestro"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="Hecho victimizante creado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param HechoVictimizante $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(HechoVictimizanteRequest $request)
    {
        if(auth()->user()->hasRole(User::ROLE_ADMIN) || auth()->user()->hasRole(User::ROLE_CASOS)) {
            try {
                $hechoVictimizante = new HechoVictimizante();
                $hechoVictimizante->fill($request->all());
                $hechoVictimizante->save();
                return response()->json($hechoVictimizante, 201);
            } catch (\Exception $exception) {
                return response()->json(['error' => $exception->getMessage()], 500);
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }

    /**
     * @OA\Put(
     *     tags={"hecho_victimizante"},
     *     path="/api/hechos_victimizantes/{id}",
     *     summary="actualizar un hecho victimizante",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del hecho victimizante a actualizar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="Atropello"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Hecho victimizante actualizado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param HechoVictimizante $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(HechoVictimizanteRequest $request, $id)
    {
        $respuesta = null;
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $hechoVictimizante = HechoVictimizante::query()->findOrFail($id);
                $hechoVictimizante->fill($request->all());
                $hechoVictimizante->save();
                $respuesta = response()->json($hechoVictimizante, 200);
            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }

    /**
     * @OA\Get(
     *     tags={"hecho_victimizante"},
     *     path="/api/hechos_victimizantes/{id}",
     *     summary="Retorna un hecho victimizante especifico",
     *     @OA\Parameter(
     *          name="id",
     *          description="id del hecho victimizante a mostrar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Retonar el hecho victimizante."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $respuesta = null;
        try {
            $hechoVictimizante = HechoVictimizante::query()->findOrFail($id);
            $respuesta = response()->json($hechoVictimizante);
        } catch (ModelNotFoundException $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 500);
        } finally {
            return $respuesta;
        }

    }

    /**
     * @OA\Delete(
     *     tags={"hecho_victimizante"},
     *     path="/api/hechos_victimizantes/{id}",
     *     summary="Elimina un hecho_victimizante",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del hecho victimizante a eliminar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="hecho victimizante Eliminado."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $respuesta = null;
        if(auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $hechoVictimizante = HechoVictimizante::query()->findOrFail($id);
                $existeHecho = Caso::query()->where('hecho_victimizante_id', $id)->exists();
                if (!$existeHecho) {
                    $hechoVictimizante->delete();
                    $respuesta = response()->json($hechoVictimizante);
                } else {
                    $respuesta = response()->json(['error' => 'No se puede eliminar un hecho vitimisante que este siendo usado por un caso'], 400);
                }
            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        }else{
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }

    }

}

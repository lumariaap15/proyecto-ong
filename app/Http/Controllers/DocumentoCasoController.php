<?php

namespace App\Http\Controllers;

use App\Http\Requests\DocumentoCasoRequest;
use App\Models\Caso;
use App\Models\DocumentoCasos;
use App\Models\VersionCaso;
use App\User;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DocumentoCasoController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['index']]);
    }

    public $mineTypeImagen = ['image/bmp','image/gif','image/jpeg','image/svg+xml','image/tiff','image/png'];
    public $mimeTypeAudio = ['audio/midi', 'audio/mpeg', 'audio/webm', 'audio/ogg', 'audio/wav'];
    public $mineTypePdf = ['application/pdf'];
    public $mimeTypeWord = ['application/msword','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/vnd.openxmlformats-officedocument.wordprocessingml.template'];
    public $mimeTypeExcel = ['application/vnd.ms-excel','application/vnd.ms-excel','application/vnd.ms-excel','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/vnd.openxmlformats-officedocument.spreadsheetml.template'];
    public $mimeTypeVideo = ['video/x-flv','video/mp4','video/3gpp','video/quicktime','video/x-msvideo','video/x-ms-wmv','video/webm', 'video/ogg'];

    /**
     * @OA\Get(
     *     tags={"documento casos"},
     *     path="/api/documentoCaso/{tipo}/{id}",
     *     summary="Muestra todas los documentos",
     *     @OA\Parameter(
     *          name="tipo",
     *          description="tipo puede ser caso o observacion, sirbe para listar los documentos dependiendo de lo que se necesite (caso,observacion)",
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="id",
     *          description="id del caso o la observacion",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="nombre",
     *          description="nombre del documento",
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="tipo_documento",
     *          description="nombre del documento",
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar todos los documentos"
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index(Request $request, $tipo, $id)
    {
        $nombre = $request->has('nombre') ? $request->get('nombre') : '';
        $typo_documento = $request->has('tipo') ? $request->get('tipo') : '';
        try {
            if ($tipo == 'caso') {
                $documentos = DocumentoCasos::query()
                    ->where('caso_id', $id)
                    ->where(function ($query) use ($nombre) {
                        if ($nombre != '') {
                            $query->where('nombre', 'like', '%' . $nombre . '%');
                        }
                    })
                    ->where(function ($query) use ($typo_documento) {
                        if ($typo_documento != '') {
                            $query->where('tipo_documento', $typo_documento);
                        }
                    })
                    ->get();
            } else {
                $documentos = DocumentoCasos::query()
                    ->where('observacion_id', $id)
                    ->where(function ($query) use ($nombre) {
                        if ($nombre != '') {
                            $query->where('nombre', 'like', '%' . $nombre . '%');
                        }
                    })
                    ->where(function ($query) use ($typo_documento) {
                        if ($typo_documento != '') {
                            $query->where('tipo_documento', $typo_documento);
                        }
                    })
                    ->get();
            }
            return response()->json($documentos, 200);
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     tags={"documento casos"},
     *     path="/api/documentoCaso",
     *     summary="Agregar un documento a un caso",
     *     security={{"bearer": {}}},
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="archivo",
     *                      description="archivo a subir",
     *                      type="file",
     *                      @OA\Items(type="string", format="binary")
     *                   ),
     *                    @OA\Property(
     *                      property="nombre",
     *                      description="nombre del archivo",
     *                      type="string",
     *                      @OA\Items(type="string")
     *                   ),
     *                   @OA\Property(
     *                      property="descripcion",
     *                      description="nombre del archivo",
     *                      type="string",
     *                      @OA\Items(type="string")
     *                   ),
     *                   @OA\Property(
     *                      property="caso_id",
     *                      description="id del caso",
     *                      type="intiger",
     *                      @OA\Items(type="intiger")
     *                   ),
     *                    @OA\Property(
     *                      property="observacion_id",
     *                      description="observacion del caso",
     *                      type="intiger",
     *                      @OA\Items(type="intiger")
     *                   ),
     *                  @OA\Property(
     *                      property="tipo_documento",
     *                      description="observacion del caso",
     *                      type="string",
     *                      @OA\Items(type="string")
     *                   ),
     *                   @OA\Property(
     *                      property="comentario",
     *                      description="Cometario actualizacion, solo aplica a usuario caso",
     *                      type="string",
     *                      @OA\Items(type="string")
     *                   ),
     *               ),
     *           ),
     *       ),
     *     @OA\Response(
     *         response=201,
     *         description="Documento Agregado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param DocumentoCasoRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(DocumentoCasoRequest $request)
    {
        DB::beginTransaction();
        try {
            $valido = $this->validateArchivo($request->tipo_documento,$request->file('archivo'));
            if(!$valido){
                return response()->json(['error' => 'El formato del archivo no corresponde al tipo de archivo enviado']);
            }
            if (auth()->user()->hasRole(User::ROLE_ADMIN) || auth()->user()->hasRole(User::ROLE_CASOS)) {
                if (auth()->user()->hasRole(User::ROLE_ADMIN)) {

                    //$mimeType = $request->file('archivo')->getClientMimeType();
                    $documento = new DocumentoCasos();
                    $documento->fill($request->all());
                    $path = Storage::disk('public')->put('documentosCasos', $request->file('archivo'));
                    $documento->archivo = Storage::url($path);
                    DB::commit();
                    $documento->save();
                    return response()->json($documento, 201);
                } else {
                    $valores_nuevos = (object)[];
                    $valores_antiguos = null;
                    $path = Storage::disk('public')->put('documentosCasos', $request->file('archivo'));
                    $valores_nuevos->archivo = Storage::url($path);
                    $valores_nuevos->nombre = $request->nombre;
                    $valores_nuevos->descripcion = $request->descripcion;
                    $valores_nuevos->caso_id = $request->caso_id;
                    $valores_nuevos->observacion_id = $request->observacion_id;
                    $valores_nuevos->tipo_documento = $request->tipo_documento;
                    $caso = Caso::query()->findOrFail($request->caso_id);
                    $radicado = $caso->radicado;
                    $version = $this->storeVersion($radicado, 'documento_casos', null, 'create', $valores_antiguos, $valores_nuevos,$request->comentario);
                    $this->storeNotificacion($radicado, 'El usuario ' . auth()->user()->name . ' agregó un documento al caso ' . $radicado, $version->id);
                    DB::commit();
                    return response()->json($valores_nuevos, 201);
                }

            } else {
                DB::rollBack();
                return response()->json(['error' => 'This action is unauthorized.'], 403);
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Put(
     *     tags={"documento casos"},
     *     path="/api/documentoCaso/{id}",
     *     summary="actualizar una documento",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del documento a actualizar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="archivo",
     *                      description="archivo a subir",
     *                      type="file",
     *                      @OA\Items(type="string", format="binary")
     *                   ),
     *                    @OA\Property(
     *                      property="nombre",
     *                      description="nombre del archivo",
     *                      type="string",
     *                      @OA\Items(type="string")
     *                   ),
     *                   @OA\Property(
     *                      property="descripcion",
     *                      description="nombre del archivo",
     *                      type="string",
     *                      @OA\Items(type="string")
     *                   ),
     *                   @OA\Property(
     *                      property="caso_id",
     *                      description="id del caso",
     *                      type="intiger",
     *                      @OA\Items(type="intiger")
     *                   ),
     *                    @OA\Property(
     *                      property="observacion_id",
     *                      description="observacion del caso",
     *                      type="intiger",
     *                      @OA\Items(type="intiger")
     *                   ),
     *                  @OA\Property(
     *                      property="tipo_documento",
     *                      description="observacion del caso",
     *                      type="string",
     *                      @OA\Items(type="string")
     *                   ),
     *                   @OA\Property(
     *                      property="comentario",
     *                      description="Cometario actualizacion, solo aplica a usuario caso",
     *                      type="string",
     *                      @OA\Items(type="string")
     *                   ),
     *               ),
     *           ),
     *       ),
     *     @OA\Response(
     *         response=200,
     *         description="Documento actualizado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param DocumentoCasoRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(DocumentoCasoRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            if($request->has('archivo') && $request->filled('archivo') && ($request->archivo !=null || $request->archivo != "")){
                $valido = $this->validateArchivo($request->tipo_documento,$request->file('archivo'));
                if(!$valido){
                    return response()->json(['error' => 'El formato del archivo no corresponde al tipo de archivo enviado']);
                }
            }
            if (auth()->user()->hasRole(User::ROLE_CASOS) || auth()->user()->hasRole(User::ROLE_ADMIN)) {
                $documento = DocumentoCasos::query()->findOrFail($id);
                $docAnterior = $documento->archivo;
                if (auth()->user()->hasRole(User::ROLE_ADMIN)) {
                    //$documento->fill($request->all());
                    if($request->has('nombre') && $request->filled('nombre')){
                        $documento->nombre = $request->nombre;
                    }
                    if($request->has('descripcion') && $request->filled('descripcion')){
                        $documento->descripcion = $request->descripcion;
                    }
                    if($request->has('caso_id') && $request->filled('caso_id')){
                        $documento->caso_id = $request->caso_id;
                    }
                    if($request->has('observacion_id') && $request->filled('observacion_id')){
                        $documento->observacion_id = $request->observacion_id;
                    }
                    if($request->has('tipo_documento') && $request->filled('tipo_documento')){
                        $documento->tipo_documento = $request->tipo_documento;
                    }
                    if ($request->has('archivo') && $request->filled('archivo') && ($request->archivo !=null || $request->archivo != "")) {
                        $path = Storage::disk('public')->put('documentosCasos', $request->file('archivo'));
                        $documento->archivo = Storage::url($path);
                        $pathanterior = str_replace('/storage', '', $docAnterior);
                        if (Storage::disk('public')->exists($pathanterior)) {
                            (Storage::disk('public')->delete($pathanterior));
                        }
                    }
                    $documento->save();
                } else {
                    $valores_nuevos = (object)[];
                    $valores_antiguos = (object)[];
                    $radicado = $documento->caso->radicado;
                    $existe = VersionCaso::query()
                        ->where('numero_radicado',$radicado)
                        ->where('nombre_tabla','documento_casos')
                        ->where('tabla_id',$documento->id)
                        //->where('accion','update')
                        //->where('user_id',auth()->id())
                        ->where('estado','pendiente')
                        ->exists();
                    if($existe){
                        return  response()->json(['error','Tiene una actualizacion pendiente por aprobación'],400);
                    }

                    if ($request->has('archivo') && $request->filled('archivo') && ($request->archivo !=null || $request->archivo != "")) {
                        $valores_antiguos->archivo = $docAnterior;
                        $path = Storage::disk('public')->put('documentosCasos', $request->file('archivo'));
                        $valores_nuevos->archivo = Storage::url($path);
                    }
                    if ($request->has('nombre') && $request->filled('nombre') && ($request->nombre != $documento->nombre)) {
                        $valores_antiguos->nombre = $documento->nombre;
                        $valores_nuevos->nombre = $request->nombre;
                    }
                    if ($request->has('descripcion') && $request->filled('descripcion') && ($documento->descripcion != $request->descripcion)) {
                        $valores_antiguos->descripcion = $documento->descripcion;
                        $valores_nuevos->descripcion = $request->descripcion;
                    }
                    if ($request->has('tipo_documento') && $request->filled('tipo_documento') && ($documento->tipo_documento != $request->tipo_documento)) {
                        $valores_antiguos->tipo_documento = $documento->tipo_documento;
                        $valores_nuevos->tipo_documento = $request->tipo_documento;
                    }

                    $version = $this->storeVersion($radicado, 'documento_casos', $documento->id, 'update', $valores_antiguos, $valores_nuevos,$request->comentario);
                    $this->storeNotificacion($radicado, 'El usuario ' . auth()->user()->name . ' actualizó un documento del caso ' . $radicado, $version->id);
                }
                DB::commit();
                return response()->json($documento, 200);
            } else {
                DB::rollBack();
                return response()->json(['error' => 'This action is unauthorized.'], 403);
            }
        } catch (ModelNotFoundException $exception) {
            DB::rollBack();
            return response()->json([
                'error' => $exception->getMessage()
            ], 404);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);

        }
    }

    /**
     * @OA\Delete(
     *     tags={"documento casos"},
     *     path="/api/documentoCasos/{id}",
     *     summary="Elimina un documento",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del documento a eliminar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="comentario", type="string", example="Cometario actualizacion, solo aplica a usuario caso"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Documento Eliminado."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            if (auth()->user()->hasRole(User::ROLE_CASOS) || auth()->user()->hasRole(User::ROLE_ADMIN)) {
                $documento = DocumentoCasos::query()->findOrFail($id);
                if (auth()->user()->hasRole(User::ROLE_ADMIN)) {
                    $documento->delete();
                } else {
                    $radicado = $documento->caso->radicado;
                    $existe = VersionCaso::query()
                        ->where('numero_radicado',$radicado)
                        ->where('nombre_tabla','documento_casos')
                        ->where('tabla_id',$documento->id)
                        //->where('accion','delete')
                        ->where('estado','pendiente')
                        ->exists();
                    if($existe){
                        return  response()->json(['error'=>'Tiene una actualizacion pendiente por aprobación'],400);
                    }
                    $version = $this->storeVersion($radicado, 'documento_casos', $documento->id, 'delete', $documento, null, $request->comentario);
                    $this->storeNotificacion($radicado, 'El usuario ' . auth()->user()->name . ' eliminó un documento del caso ' . $radicado, $version->id);
                }
                DB::commit();
                return response()->json($documento);
            } else {
                DB::rollBack();
                return response()->json(['error' => 'This action is unauthorized.'], 403);
            }
        } catch (ModelNotFoundException $exception) {
            DB::rollBack();
            return response()->json([
                'error' => $exception->getMessage()
            ], 404);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }


    private function validateArchivo($tipo_doumneto, $archivo){
        $mimeType = $archivo->getClientMimeType();
        $colletion = [];
        switch ($tipo_doumneto){
            case 'PDF':
                $colletion = collect($this->mineTypePdf);break;
            case 'IMAGEN':
                $colletion = collect($this->mineTypeImagen);break;
            case 'WORD':
                $colletion = collect($this->mimeTypeWord);break;
            case 'EXCEL':
                $colletion = collect($this->mimeTypeExcel);break;
            case 'AUDIO':
                $colletion = collect($this->mimeTypeAudio);break;
            case 'VIDEO':
                $colletion = collect($this->mimeTypeVideo);break;
        }
        return $colletion->contains($mimeType);
    }
}

<?php

namespace App\Http\Controllers;

use Artisan;
use Storage;


class BackupController extends Controller
{


    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['download']]);
    }

    /**
     * @OA\Get(
     *     tags={"Backup"},
     *     path="/api/backup",
     *     summary="Lista los backup generados",
     *     security={{"bearer": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Listado de backups"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="usuario no autenticado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index()
    {
        $disk = Storage::disk(config('backup.backup.destination.disks')[0]);

        $files = $disk->files(config('backup.backup.name'));
        $backups = [];
        // make an array of backup files, with their filesize and creation date
        foreach ($files as $k => $f) {
            // only take the zip files into account
            if (substr($f, -4) == '.zip' && $disk->exists($f)) {
                $backups[] = [
                    'file_path' => $f,
                    'file_name' => str_replace(config('backup.backup.name') . '/', '', $f),
                    'file_size' => $disk->size($f),
                    'last_modified' => $disk->lastModified($f),
                ];
            }
        }
        // reverse the backups, so the newest one would be on top
        $backups = array_reverse($backups);
        return response()->json($backups);
    }


    /**
     * @OA\Post(
     *     tags={"Backup"},
     *     path="/api/backup",
     *     summary="Genera un nuevo backup",
     *     security={{"bearer": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Backup generado"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="usuario no autenticado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function store()
    {
        try {
            set_time_limit(-1);
            Artisan::call('backup:run', ['--disable-notifications'=>true]);
            $output = Artisan::output();
            return response()->json($output);
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }
    }


    /**
     * @OA\Get(
     *     tags={"Backup"},
     *     path="/api/backup/download/{file_name}",
     *     summary="Descarga un backup",
     *     @OA\Parameter(
     *          name="file_name",
     *          description="nombre del backup que se desea descargar",
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Backup descargado"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="usuario no autenticado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function download($file_name)
    {
        $file = config('backup.backup.name') . '/' . $file_name;
        $disk = Storage::disk(config('backup.backup.destination.disks')[0]);
        if ($disk->exists($file)) {
            $fs = Storage::disk(config('backup.backup.destination.disks')[0])->getDriver();
            $stream = $fs->readStream($file);

            return \Response::stream(function () use ($stream) {
                fpassthru($stream);
            }, 200, [
                "Content-Type" => $fs->getMimetype($file),
                "Content-Length" => $fs->getSize($file),
                "Content-disposition" => "attachment; filename=\"" . basename($file) . "\"",
            ]);
        } else {
            abort(404, "The backup file doesn't exist.");
        }
    }

    /**
     * @OA\Delete(
     *     tags={"Backup"},
     *     path="/api/backup/{file_name}",
     *     summary="Elimina un backup",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="file_name",
     *          description="nombre del backup que se desea eliminar",
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Backup eliminado"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="usuario no autenticado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function delete($file_name)
    {
        $disk = Storage::disk(config('backup.backup.destination.disks')[0]);
        if ($disk->exists(config('backup.backup.name') . '/' . $file_name)) {
            $disk->delete(config('backup.backup.name') . '/' . $file_name);
            return response()->json('Archivo eliminado', 200);
        } else {
            return response()->json('El archivo a eliminar no existe', 404);
        }
    }
}

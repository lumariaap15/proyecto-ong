<?php

namespace App\Http\Controllers;

use App\Http\Requests\ResponsableHechoRequest;
use App\Models\Caso;
use App\Models\ResponsableHecho;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ResponsableHechoController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['index', 'show']]);
    }

    /**
     * @OA\Get(
     *     tags={"Responsables_hecho"},
     *     path="/api/responsableHecho",
     *     summary="Muestra todos los responsables de hechos que se hallan registrado ",
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar Responsables de hecho."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index()
    {
        try {
            $resposable = ResponsableHecho::all();
            return response()->json($resposable, 200);
        } catch (\Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);

        }
    }

    /**
     * @OA\Post(
     *     tags={"Responsables_hecho"},
     *     path="/api/responsableHecho",
     *     summary="Crea un resposables de un hecho",
     *     security={{"bearer": {}}},
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="Guerrilla"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="Responsable del caso creado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param ResponsableHechoRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ResponsableHechoRequest $request)
    {
        if (auth()->user()->hasRole(User::ROLE_ADMIN) || auth()->user()->hasRole(User::ROLE_CASOS)) {
            try {
                $responsable = new ResponsableHecho();
                $responsable->fill($request->all());
                $responsable->save();
                return response()->json($responsable, 201);
            } catch (\Exception $exception) {
                return response()->json(['error' => $exception->getMessage()], 500);
            }
        } else {
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }

    /**
     * @OA\Put(
     *      tags={"Responsables_hecho"},
     *     path="/api/responsableHecho/{id}",
     *     summary="actualizar un responsable de un hecho",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del responsable del hecho a actualizar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="descripcion", type="string", example="Bacrim"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Responsable del hecho actualizado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param ResponsableHechoRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ResponsableHechoRequest $request, $id)
    {
        $respuesta = null;
        if (auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $responsable = ResponsableHecho::query()->findOrFail($id);
                $responsable->fill($request->all());
                $responsable->save();
                $respuesta = response()->json($responsable, 200);
            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        } else {
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }

    /**
     * @OA\Get(
     *     tags={"Responsables_hecho"},
     *     path="/api/responsableHecho/{id}",
     *     summary="Retorna un responsable del hecho especifico",
     *     @OA\Parameter(
     *          name="id",
     *          description="id del responsable del hecho a mostrar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Retonar el responsable del hecho."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $respuesta = null;
        try {
            $responsable = ResponsableHecho::query()->findOrFail($id);
            $respuesta = response()->json($responsable);
        } catch (ModelNotFoundException $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 500);
        } finally {
            return $respuesta;
        }

    }

    /**
     * @OA\Delete(
     *     tags={"Responsables_hecho"},
     *     path="/api/responsableHecho/{id}",
     *     summary="Elimina un responsable del hecho",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del responsable del hecho eliminar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Responsable del hecho Eliminado."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $respuesta = null;
        if (auth()->user()->hasRole(User::ROLE_ADMIN)) {
            try {
                $responsable = ResponsableHecho::query()->findOrFail($id);
                $existCaso = Caso::query()->where('responsable_del_hecho_id', $id)->exists();
                if (!$existCaso) {
                    $responsable->delete();
                    $respuesta = response()->json($responsable);
                } else {
                    $respuesta = response()->json(['error' => 'No se puede eliminar'], 400);
                }
            } catch (ModelNotFoundException $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        } else {
            return response()->json(['error' => 'This action is unauthorized.'], 401);
        }
    }

}


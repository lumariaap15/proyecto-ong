<?php

namespace App\Http\Controllers;

use App\Http\Requests\CasoRequest;
use App\Models\Caso;
use App\Models\Comunidad;
use App\Models\Departamento;
use App\Models\EstadoProcesoCaso;
use App\Models\HechoVictimizante;
use App\Models\Municipio;
use App\Models\Organizacion;
use App\Models\Persona;
use App\Models\ResponsableHecho;
use App\Models\VersionCaso;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Validator;

class CasoController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['index', 'show']]);
    }

    /**
     * @OA\Get(
     *     tags={"casos"},
     *     path="/api/caso",
     *     summary="Muestra todas los casos",
     *     @OA\Parameter(
     *          name="page",
     *          description="numero de pagina, enviar solo si se desea paginar, de lo contrario regresa todos los registros",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="id",
     *          description="buscar el caso por id",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),@OA\Parameter(
     *          name="fecha_del_hecho",
     *          description="buscar el caso por fecha del hecho",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="departamento",
     *          description="codigo del departamento a buscar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="municipio",
     *          description="codigo del municipio a buscar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ), @OA\Parameter(
     *          name="hecho_victimizante",
     *          description="id del hecho victimizante a buscar",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="victima",
     *          description="victima id",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="denunciante",
     *          description="denunciante id",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="tipo_victima",
     *          description="tipo de victima si es una persona o una comunidad",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="fecha_de_inicio",
     *          description="fecha de inicio del rango de busqueda",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="fecha_final",
     *          description="fecha final del rango de busqueda",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar los casos."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index(Request $request)
    {
        try {

            $radicado = $request->has('radicado') ? $request->get('radicado') : '';
            $fecha_de_inicio = $request->has('fecha_de_inicio') ? $request->get('fecha_de_inicio') : '';
            $fecha_final = $request->has('fecha_final') ? $request->get('fecha_final') : '';
            $departamento = $request->has('departamento') ? $request->get('departamento') : '';
            $municipio = $request->has('municipio') ? $request->get('municipio') : '';
            $hecho_victimizante = $request->has('hecho_victimizante') ? $request->get('hecho_victimizante') : '';

            $victima = $request->has('victima') ? $request->get('victima') : '';
            $denunciante = $request->has('denunciante') ? $request->get('denunciante') : '';
            $tipo_victima = $request->has('tipo_victima') ? $request->get('tipo_victima') : '';

            if ($request->has('page')) {
                if ($radicado != '') {
                    $caso = Caso::with('departamento', 'municipios', 'hecho_victimizante', 'fecha_del_hecho_one', 'comunidad', 'victima', 'denunciante')
                        ->where('radicado', $radicado)->paginate(10);
                } else {
                    $caso = Caso::with('departamento', 'municipios', 'hecho_victimizante', 'fecha_del_hecho_one', 'comunidad', 'victima', 'denunciante')->where(function ($query) use ($tipo_victima, $victima) {
                        if ($victima != '') {
                            if ($tipo_victima == 'persona') {
                                $query->where('victima_id', $victima);
                            } else {
                                $query->where('comunidad_id', $victima);
                            }
                        }
                    })
                        ->where(function ($query) use ($denunciante) {
                            if ($denunciante != '') {
                                $query->where('denunciante_id', $denunciante);
                            }
                        })
                        ->where(function ($query) use ($municipio) {
                            if ($municipio != '') {
                                $query->where('municipio_id', $municipio);
                            }
                        })
                        ->where(function ($query) use ($departamento) {
                            if ($departamento != '') {
                                if (strlen($departamento) == 1) {
                                    $departamento = '0' . $departamento;
                                }
                                $query->where('departamento_codigo', $departamento);
                            }
                        })
                        ->where(function ($query) use ($hecho_victimizante) {
                            if ($hecho_victimizante != '') {
                                $query->where('hecho_victimizante_id', $hecho_victimizante);
                            }
                        })
                        ->where(function ($query) use ($fecha_de_inicio, $fecha_final) {
                            if ($fecha_de_inicio != '' && $fecha_final != '') {
                                $query->whereHas('fechas', function ($query2) use ($fecha_de_inicio, $fecha_final) {
                                    $fecha_de_inicio = Carbon::createFromFormat('Y-m-d', $fecha_de_inicio);
                                    $fecha_final = Carbon::createFromFormat('Y-m-d', $fecha_final);
                                    $query2->whereDate('fecha_inicio', '>=', $fecha_de_inicio);
                                    $query2->whereDate('fecha_final', '<=', $fecha_final);
                                });
                            }
                        })
                        ->paginate(10);
                }

            } else {
                if ($radicado != '') {
                    $caso = Caso::with('departamento', 'municipios', 'hecho_victimizante', 'fecha_del_hecho_one', 'comunidad', 'victima', 'denunciante')->where('radicado', $radicado)->paginate(10);
                } else {
                    $caso = Caso::with('departamento', 'municipios', 'hecho_victimizante', 'fecha_del_hecho_one', 'comunidad', 'victima', 'denunciante')->where(function ($query) use ($tipo_victima, $victima) {
                        if ($victima != '') {
                            if ($tipo_victima == 'persona') {
                                $query->where('victima_id', $victima);
                            } else {
                                $query->where('comunidad_id', $victima);
                            }
                        }
                    })
                        ->where(function ($query) use ($denunciante) {
                            if ($denunciante != '') {
                                $query->where('denunciante_id', $denunciante);
                            }
                        })
                        ->where(function ($query) use ($municipio) {
                            if ($municipio != '') {
                                $query->where('municipio_id', $municipio);
                            }
                        })
                        ->where(function ($query) use ($departamento) {
                            if ($departamento != '') {
                                $query->where('departamento_codigo', $departamento);
                            }
                        })
                        ->where(function ($query) use ($hecho_victimizante) {
                            if ($hecho_victimizante != '') {
                                $query->where('hecho_victimizante_id', $hecho_victimizante);
                            }
                        })
                        ->where(function ($query) use ($fecha_de_inicio, $fecha_final) {
                            if ($fecha_de_inicio != '' && $fecha_final != '') {
                                $query->whereHas('fechas', function ($query2) use ($fecha_de_inicio, $fecha_final) {
                                    $fecha_de_inicio = Carbon::createFromFormat('Y-m-d', $fecha_de_inicio);
                                    $fecha_final = Carbon::createFromFormat('Y-m-d', $fecha_final);
                                    $query2->whereDate('fecha_inicio', '>=', $fecha_de_inicio);
                                    $query2->whereDate('fecha_final', '<=', $fecha_final);
                                });
                            }
                        })->get();
                }
            }

            return response()->json($caso, 200);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
    }

    /**
     * @OA\Post(
     *     tags={"casos"},
     *     path="/api/caso",
     *     summary="Crea un caso",
     *     security={{"bearer": {}}},
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="denunciante_id", type="integer", example="1"),
     *         @OA\Property(property="victima_id", type="integer", example="1"),
     *         @OA\Property(property="tipo_victima", type="string", example="comunidad"),
     *         @OA\Property(property="organizacion_victima_id", type="integer", example="1"),
     *         @OA\Property(property="comunidad", type="string", example="1"),
     *         @OA\Property(property="hecho_victimizante_id", type="integer", example="1"),
     *         @OA\Property(property="fecha_de_recepcion", type="string", example="1985-08-18"),
     *         @OA\Property(property="responsable_del_hecho", type="string", example="guerrilla"),
     *         @OA\Property(property="municipio_id", type="string", example="50001"),
     *         @OA\Property(property="departamento_codigo", type="string", example="50"),
     *         @OA\Property(property="direccion", type="string", example="mz 80 casa 12"),
     *         @OA\Property(property="relato", type="string", example="relato del suceso"),
     *         @OA\Property(property="tipo_fecha", type="string", example="Mes"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="Caso creado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param CasoRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $validator = Validator::make(collect($request->info_caso)->toArray(), [
            'departamento_codigo' => 'required',
            'fecha_de_recepcion' => 'required',
            'direccion' => 'nullable',
            'hecho_victimizante_id' => 'required|numeric',
            'municipio_codigo' => 'required',
            'fechas' => 'required|array',
            'cantidad_personas_afectadas' => 'required|numeric',
            'estado_proceso_caso_id' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return response()->json(
                [
                    'error' => $validator->errors()->all()[0],
                ],
                422
            );
        }
        if (auth()->user()->hasRole(User::ROLE_ADMIN) || auth()->user()->hasRole(User::ROLE_CASOS)) {
            DB::beginTransaction();
            try {
                $caso = new Caso();

                $caso->denunciante_id = $request->denunciante_id;
                //verifica si el tipo de victima es comunnidad o persona.
                if ($request->tipo_victima === 'comunidad') {
                    $caso->comunidad_id = $request->victima_id;
                } else {
                    if ($request->tipo_victima === 'persona') {
                        $caso->victima_id = $request->victima_id;
                    }
                }
                $caso->organizacion_victima_id = $request->organizacion_victima_id;
                $caso->departamento_codigo = $request->info_caso['departamento_codigo'];
                /*if (strlen($caso->departamento_codigo) == 1) {
                    $caso->departamento_codigo = '0' . $caso->departamento_codigo;
                }*/
                $caso->fecha_de_recepcion = $request->info_caso['fecha_de_recepcion'];
                $caso->direccion = $request->info_caso['direccion'];
                $caso->hecho_victimizante_id = $request->info_caso['hecho_victimizante_id'];
                $caso->municipio_id = $request->info_caso['municipio_codigo'];
                $caso->relato = $request->info_caso['relato'];
                $caso->responsable_del_hecho_id = $request->info_caso['responsable_del_hecho_id'];
                $caso->cantidad_personas_afectadas = $request->info_caso['cantidad_personas_afectadas'];
                $caso->estado_proceso_caso_id = $request->info_caso['estado_proceso_caso_id'];
                //$caso->tipo_fecha = $request->info_caso['tipo_fecha'];

                $ultimoCaso = Caso::query()->latest()->first();
                $codigo_final = $caso->departamento_codigo;
                $hecho = $caso->hecho_victimizante->id;
                if (strlen($codigo_final) == 1) {
                    $codigo_final = '0' . $codigo_final;
                }

                if ($ultimoCaso) {
                    $consecutivo = substr($ultimoCaso->radicado, (strlen($ultimoCaso->radicado)) - 6, 6);
                    $consecutivo++;
                    $catida_digitos = strlen($consecutivo);
                    if ($catida_digitos < 6) {
                        $faltan = 6 - $catida_digitos;
                        for ($i = 0; $i < $faltan; $i++) {
                            $consecutivo = '0' . $consecutivo;
                        }
                    }

                    $caso->radicado = Carbon::now()->format('Ymd') . $codigo_final . $caso->municipio_id .$hecho. $consecutivo;
                } else {
                    $caso->radicado = Carbon::now()->format('Ymd') . $codigo_final . $caso->municipio_id .$hecho. '000001';
                }
                $caso->save();
                foreach ($request->info_caso['fechas'] as $fecha) {
                    $caso->fechas()->create([
                        'tipo_fecha' => $fecha['tipo_fecha'],
                        'fecha_inicio' => $fecha['fecha_inicio'],
                        'fecha_final' => $fecha['fecha_final']
                    ]);
                }
                DB::commit();
                return response()->json($caso, 201);
            } catch (\Exception $exception) {
                DB::rollback();
                return response()->json([
                    'error' => $exception->getMessage()
                ], 500);
            }
        } else {
            return response()->json(['error' => 'This action is unauthorized.'], 403);
        }
    }

    /**
     * @OA\Get(
     *     tags={"casos"},
     *     path="/api/caso/{id}",
     *     summary="Retorna caso especifico especifico",
     *     @OA\Parameter(
     *          name="id",
     *          description="id del caso a mostrar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Retonar un caso."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $respuesta = null;
        try {
            $caso = Caso::with('fechas', 'departamento', 'municipios', 'hecho_victimizante')->findOrFail($id);
            $respuesta = response()->json($caso);
        } catch (ModelNotFoundException $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            $respuesta = response()->json(['error' => $exception->getMessage()], 500);
        } finally {
            return $respuesta;
        }
    }

    /**
     * @OA\Put(
     *     tags={"casos"},
     *     path="/api/caso/{id}",
     *     summary="actualizar un caso",
     *     security={{"bearer": {}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="id del caso a actualizar",
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *        @OA\Property(property="denunciante_id", type="integer", example="3"),
     *         @OA\Property(property="victima_id", type="integer", example="2"),
     *         @OA\Property(property="organizacion_victima_id", type="integer", example="1"),
     *         @OA\Property(property="comunidad_id", type="integer", example="2"),
     *         @OA\Property(property="hecho_victimizante_id", type="integer", example="1"),
     *         @OA\Property(property="fecha_de_recepcion", type="string", example="1985-08-18"),
     *         @OA\Property(property="fecha_del_hecho", type="string", example="2018-10-25"),
     *         @OA\Property(property="responsable_del_hecho", type="string", example="bacrin"),
     *         @OA\Property(property="municipio_id", type="string", example="50001"),
     *         @OA\Property(property="departamento_codigo", type="string", example="50"),
     *         @OA\Property(property="direccion", type="string", example="mz 80 casa 12"),
     *         @OA\Property(property="relato", type="string", example="relato del suceso"),
     *         @OA\Property(property="tipo_fecha", type="string", example="Periodo"),
     *         @OA\Property(property="comentario", type="string", example="Cometario actualizacion, solo aplica a usuario caso"),
     *       )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="caso actualizado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Error en los datos del formulario."
     *     ),
     *      @OA\Response(
     *         response=404,
     *         description="Recurso no encontrado."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * @param CasoRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(CasoRequest $request, $id)
    {
        $respuesta = null;
        $validator = Validator::make($request->info_caso, [
            'departamento_codigo' => 'nullable',
            'fecha_de_recepcion' => 'nullable',
            'direccion' => 'nullable',
            'hecho_victimizante_id' => 'nullable|numeric',
            'municipio_codigo' => 'nullable',
            'cantidad_personas_afectadas' => 'nullable|numeric',
            'estado_proceso_caso_id' => 'nullable|numeric'
        ]);
        if ($validator->fails()) {
            return response()->json(
                [
                    'error' => $validator->errors()->all()[0],
                ],
                422
            );
        }
        if (auth()->user()->hasRole(User::ROLE_ADMIN) || auth()->user()->hasRole(User::ROLE_CASOS)) {
            DB::beginTransaction();
            try {
                $caso = Caso::query()->findOrFail($id);
                if (auth()->user()->hasRole(User::ROLE_ADMIN)) {
                    if ($request->has('denunciante_id') && $request->filled('denunciante_id')) {
                        $caso->denunciante_id = $request->denunciante_id;
                    }
                    //verifica si el tipo de victima es comunnidad o persona.
                    if ($request->has('tipo_victima') && $request->filled('tipo_victima')) {
                        if ($request->tipo_victima === 'comunidad') {
                            $caso->comunidad_id = $request->victima_id;
                        } else {
                            if ($request->tipo_victima === 'persona') {
                                $caso->victima_id = $request->victima_id;
                            }
                        }
                    }
                    if ($request->has("organizacion_victima_id") && $request->filled('organizacion_victima_id')) {
                        $caso->organizacion_victima_id = $request->organizacion_victima_id;
                    }
                    if (isset($request->info_caso['departamento_codigo'])) {
                        $caso->departamento_codigo = $request->info_caso['departamento_codigo'];
                        if (strlen($caso->departamento_codigo) == 1) {
                            $caso->departamento_codigo = '0' . $caso->departamento_codigo;
                        }
                    }
                    if (isset($request->info_caso['fecha_de_recepcion'])) {
                        $caso->fecha_de_recepcion = $request->info_caso['fecha_de_recepcion'];
                    }
                    if (isset($request->info_caso['direccion'])) {
                        $caso->direccion = $request->info_caso['direccion'];
                    }
                    if (isset($request->info_caso['hecho_victimizante_id'])) {
                        $caso->hecho_victimizante_id = $request->info_caso['hecho_victimizante_id'];
                    }
                    if (isset($request->info_caso['municipio_codigo'])) {
                        $caso->municipio_id = $request->info_caso['municipio_codigo'];
                    }
                    if (isset($request->info_caso['relato'])) {
                        $caso->relato = $request->info_caso['relato'];
                    }
                    if (isset($request->info_caso['responsable_del_hecho_id'])) {
                        $caso->responsable_del_hecho_id = $request->info_caso['responsable_del_hecho_id'];
                    }
                    if (isset($request->info_caso['cantidad_personas_afectadas'])) {
                        $caso->cantidad_personas_afectadas = $request->info_caso['cantidad_personas_afectadas'];
                    }
                    //$caso->fill($request->all());
                    if ($caso->isDirty()) {
                        $caso->save();
                    }
                } else {
                    $existe = VersionCaso::query()
                        ->where('numero_radicado', $caso->radicado)
                        ->where('nombre_tabla', 'casos')
                        ->where('accion', 'update')
                        ->where('user_id', auth()->id())
                        ->where('estado', 'pendiente')
                        ->exists();
                    if ($existe) {
                        return response()->json(['error', 'Tiene una actualizacion pendiente por aprobacion'], 400);
                    }
                    $valores_nuevos = (object)[];
                    $valores_antiguos = (object)[];
                    if ($request->has('denunciante_id') && $request->filled('denunciante_id') && ($caso->denunciante_id != $request->denunciante_id)) {
                        $perosna = Persona::query()->findOrFail($request->denunciante_id);
                        $valores_nuevos->denunciante_id = ['denunciante_id' => $request->denunciante_id, 'nombre' => $perosna->nombres . ' ' . $perosna->apellido];
                        $valores_antiguos->denunciante_id = ['denunciante_id' => $caso->denunciante_id, 'nombre' => $caso->denunciante->nombres . ' ' . $caso->denunciante->apellido];
                    }
                    //verifica si el tipo de victima es comunnidad o persona.
                    if ($request->has('tipo_victima') && $request->filled('tipo_victima')) {
                        if ($request->tipo_victima === 'comunidad' && ($caso->comunidad_id != $request->victima_id)) {
                            $comunidad = Comunidad::query()->findOrFail($request->comunidad_id);
                            $valores_nuevos->comunidad_id = ['comunidad_id' => $request->victima_id, 'nombre' => $comunidad->descripcion];
                            $valores_antiguos->comunidad_id = ['comunidad_id' => $caso->comunidad_id, 'nombre' => $caso->cominidad->descripcion];
                        } else {
                            if ($request->tipo_victima === 'persona' && ($caso->victima_id != $request->victima_id)) {
                                $perosna = Persona::query()->findOrFail($request->victima_id);
                                $valores_nuevos->victima_id = ['victima_id' => $request->victima_id, 'nombre' => $perosna->nombres . ' ' . $perosna->apellido];
                                $valores_antiguos->victima_id = ['victima_id' => $caso->victima_id, 'nombre' => $caso->victima->nombres . ' ' . $caso->victima->apellido];
                            }
                        }
                    }
                    if ($request->has('organizacion_victima_id') && $request->filled('organizacion_victima_id') && ($caso->organizacion_victima_id != $request->organizacion_victima_id)) {
                        $organizacion = Organizacion::query()->findOrFail($request->organizacion_victima_id);
                        $valores_nuevos->organizacion_victima_id = ['organizacion_victima_id' => $request->organizacion_victima_id, 'nombre' => $organizacion->descripcion];
                        $valores_antiguos->organizacion_victima_id = ['organizacion_victima_id' => $caso->organizacion_victima_id, 'nombre' => $caso->organizacions->descripcion];
                    }
                    if (isset($request->info_caso['departamento_codigo']) && ($caso->departamento_codigo != $request->info_caso['departamento_codigo'])) {
                        $departamento_codigo = $request->info_caso['departamento_codigo'];
                        if (strlen($departamento_codigo) == 1) {
                            $departamento_codigo = '0' . $departamento_codigo;
                        }
                        $departamentoCon = Departamento::query()->where('codigo', $departamento_codigo)->first();
                        $valores_nuevos->departamento_codigo = ['departamento_codigo' => $departamento_codigo, 'nombre' => $departamentoCon->descripcion];
                        $valores_antiguos->departamento_codigo = ['departamento_codigo' => $caso->departamento_codigo, 'nombre' => $caso->departamento->descripcion];
                    }
                    if (isset($request->info_caso['fecha_de_recepcion']) && ($caso->fecha_de_recepcion != $request->info_caso['fecha_de_recepcion'])) {
                        $valores_nuevos->fecha_de_recepcion = $request->info_caso['fecha_de_recepcion'];
                        $valores_antiguos->fecha_de_recepcion = $caso->fecha_de_recepcion;
                    }
                    if (isset($request->info_caso['direccion']) && ($caso->direccion != $request->info_caso['direccion'])) {
                        $valores_nuevos->direccion = $request->info_caso['direccion'];
                        $valores_antiguos->direccion = $caso->direccion;
                    }
                    if (isset($request->info_caso['hecho_victimizante_id']) && ($caso->hecho_victimizante_id != $request->info_caso['hecho_victimizante_id'])) {
                        $hechoVitimisante = HechoVictimizante::query()->findOrFail($request->info_caso['hecho_victimizante_id']);
                        $valores_nuevos->hecho_victimizante_id = ['hecho_victimizante_id' => $request->info_caso['hecho_victimizante_id'], 'nombre' => $hechoVitimisante->descripcion];
                        $valores_antiguos->hecho_victimizante_id = ['hecho_victimizante_id' => $caso->hecho_victimizante_id, 'nombre' => $caso->hecho_victimizante->descripcion];
                    }
                    if (isset($request->info_caso['estado_proceso_caso_id']) && ($caso->estado_proceso_caso_id != $request->info_caso['estado_proceso_caso_id'])) {
                        $estadoProceso = EstadoProcesoCaso::query()->findOrFail($request->info_caso['estado_proceso_caso_id']);
                        $valores_nuevos->estado_proceso_caso_id = ['estado_proceso_caso_id' => $request->info_caso['estado_proceso_caso_id'], 'nombre' => $estadoProceso->descripcion];
                        $valores_antiguos->estado_proceso_caso_id = ['estado_proceso_caso_id' => $caso->estado_proceso_caso_id, 'nombre' => $caso->estado_proceso->descripcion];
                    }
                    if (isset($request->info_caso['municipio_codigo']) && ($caso->municipio_id != $request->info_caso['municipio_codigo'])) {
                        $municipio = Municipio::query()->where('codigo', $request->info_caso['municipio_codigo'])->first();
                        $valores_nuevos->municipio_id = ['municipio_id' => $request->info_caso['municipio_codigo'], 'nombre' => $municipio->descripcion];
                        $valores_antiguos->municipio_id = ['municipio_id' => $caso->municipio_id, 'nombre' => $caso->municipios->descripcion];
                    }
                    if (isset($request->info_caso['relato']) && ($caso->relato != $request->info_caso['relato'])) {
                        $valores_nuevos->relato = $request->info_caso['relato'];
                        $valores_antiguos->relato = $caso->relato;
                    }
                    if (isset($request->info_caso['responsable_del_hecho_id']) && ($caso->responsable_del_hecho_id != $request->info_caso['responsable_del_hecho_id'])) {
                        $responsable = ResponsableHecho::query()->findOrFail($request->info_caso['responsable_del_hecho_id']);
                        $valores_nuevos->responsable_del_hecho_id = ['responsable_del_hecho_id' => $request->info_caso['responsable_del_hecho_id'], 'nombre' => $responsable->descripcion];
                        $valores_antiguos->responsable_del_hecho_id = ['responsable_del_hecho_id' => $caso->responsable_del_hecho_id, 'nombre' => $caso->responsable->descripcion];
                    }
                    if (isset($request->info_caso['cantidad_personas_afectadas']) && ($caso->cantidad_personas_afectadas != $request->info_caso['cantidad_personas_afectadas'])) {
                        $valores_nuevos->cantidad_personas_afectadas = $request->info_caso['cantidad_personas_afectadas'];
                        $valores_antiguos->cantidad_personas_afectadas = $caso->cantidad_personas_afectadas;
                    }
                    $radicado = $caso->radicado;
                    $version = $this->storeVersion(
                        $radicado,
                        'casos',
                        $caso->id,
                        'update',
                        $valores_antiguos,
                        $valores_nuevos,
                        $request->comentario);
                    $this->storeNotificacion($radicado, 'El usuario ' . auth()->user()->name . ' realizó un cambio en el caso ' . $radicado, $version->id);
                }
                DB::commit();
                $respuesta = response()->json($caso, 200);
            } catch (ModelNotFoundException $exception) {
                DB::rollBack();
                $respuesta = response()->json(['error' => $exception->getMessage()], 404);
            } catch (\Exception $exception) {
                DB::rollBack();
                $respuesta = response()->json(['error' => $exception->getMessage()], 500);
            } finally {
                return $respuesta;
            }
        } else {
            return response()->json(['error' => 'This action is unauthorized.'], 403);
        }
    }
}

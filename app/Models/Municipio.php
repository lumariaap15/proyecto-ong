<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{

    protected $primaryKey = 'codigo';

    public function departamento(){
        return $this->belongsTo(Departamento::class,'departamento_codigo','codigo');
    }

    public function casos(){
        return $this->hasMany(Caso::class,'municipio_id','codigo');
    }

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}

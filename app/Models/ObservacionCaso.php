<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ObservacionCaso extends Model
{
    protected $fillable = [
        'descripcion',
        'user_id',
        'caso_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function caso()
    {
        return $this->belongsTo(Caso::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $primaryKey = 'codigo';
    public $incrementing = 'false';

    protected $fillable = [
      'codigo',
      'descripcion'
    ];

    public function municipios()
    {
        return $this->hasMany(Municipio::class, 'departamento_codigo', 'codigo');
    }

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}

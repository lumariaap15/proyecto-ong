<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ResponsableHecho extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'descripcion'
    ];

    public function Caso()
    {
        return $this->belongsTo(Caso::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class DocumentoCasos extends Model implements AuditableContract
{
    use Auditable, SoftDeletes;

    protected $fillable = [
        'nombre', 'archivo', 'descripcion', 'tipo_documento', 'caso_id', 'observacion_id'
    ];

    protected function caso()
    {
        return $this->belongsTo(Caso::class);
    }
}

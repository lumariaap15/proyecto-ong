<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class VersionCaso extends Model
{
    //

    public function getValorAntesAttribute($value){
        return json_decode($value,true);
    }

    public function getValorDespuesAttribute($value){
        return json_decode($value,true);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}

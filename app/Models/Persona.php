<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Persona extends Model implements AuditableContract
{
    use SoftDeletes;
    use Auditable;

    protected $fillable = [
        'tipo_documento_id',
        'n_documento',
        'nombres',
        'apellido',
        'fecha_nacimiento',
        'estado_civil_id',
        'grado_escolaridad_id',
        'genero',
        'municipio_codigo',
        'departamento_codigo',
        'grupo_etnico_id',
        'grupo_indigena_id',
        'ocupacion',
        'direccion',
        'lgtbi'
    ];


    protected $dates = [
        'fecha_nacimiento',
    ];

    protected $filasBuscar = [
        'tipo_documento_id',
        'n_documento',
        'nombres',
        //'fecha_nacimiento',
        'estado_civil_id',
        'grado_escolaridad_id',
        'genero',
        'municipio_codigo',
        'departamento_codigo',
        'grupo_etnico_id',
        'grupo_indigena_id',
        'ocupacion',
        'direccion'
    ];


    public function scopeBuscarCoincidencia($query, $termino)
    {
        return $query->whereLike($this->filasBuscar, $termino);
    }

    public function tipoDocumento()
    {
        return $this->belongsTo(TipoDocumento::class);
    }

    public function estadoCivil()
    {
        return $this->belongsTo(EstadoCivil::class);
    }

    public function gradoEscolaridad()
    {
        return $this->belongsTo(GradoEscolaridad::class);
    }

    public function municipio()
    {
        return $this->belongsTo(Municipio::class, 'municipio_codigo', 'codigo');
    }

    public function departamento()
    {
        return $this->belongsTo(Departamento::class, 'departamento_codigo', 'codigo');
    }

    public function grupoEtnico()
    {
        return $this->belongsTo(GrupoEtnico::class);
    }

    public function grupoIndigena()
    {
        return $this->belongsTo(GrupoIndigena::class);
    }


    public function casosDenunciante(){
        return $this->hasMany(Caso::class,'denunciante_id','id');
    }

    public function casosVictima(){
        return $this->hasMany(Caso::class,'victima_id','id');
    }
}

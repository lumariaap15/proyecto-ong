<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Comunidad extends Model implements AuditableContract
{
    use  Auditable;
    protected $fillable = [
        'descripcion',
        'municipio_codigo'
    ];

    public function municipios()
    {
        return $this->belongsTo(Municipio::class, 'municipio_codigo', 'codigo');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Caso extends Model implements AuditableContract
{
    use Auditable;
    protected $fillable = [
        'denunciante_id',
        'victima_id',
        'organizacion_victima_id',
        'comunidad_id',
        'hecho_victimizante_id',
        'fecha_de_recepcion',
        'responsable_del_hecho_id',
        'departamento_codigo',
        'municipio_id',
        'direccion',
        'relato',
        'tipo_fecha'

    ];
    protected $filasBuscar = [
        'denunciante_id',
        'victima_id',
        'organizacion_victima_id',
        'comunidad_id',
        'hecho_victimizante_id',
        'fecha_de_recepcion',
        'responsable_del_hecho_id',
        'departamento_codigo',
        'municipio_id',
        'direccion',
        'relato',
        'tipo_fecha'
    ];

    public function denunciante()
    {
        return $this->belongsTo(Persona::class, 'denunciante_id', 'id');
    }

    public function victima()
    {
        return $this->belongsTo(Persona::class, 'victima_id', 'id');
    }

    public function organizacions()
    {
        return $this->belongsTo(Organizacion::class, 'organizacion_victima_id', 'id');
    }

    public function comunidad()
    {
        return $this->belongsTo(Comunidad::class, 'comunidad_id', 'id');
    }

    public function hecho_victimizante()
    {
        return $this->belongsTo(HechoVictimizante::class, 'hecho_victimizante_id', 'id');

    }

    public function estado_proceso()
    {
        return $this->belongsTo(EstadoProcesoCaso::class, 'estado_proceso_caso_id', 'id');
    }

    public function municipios()
    {
        return $this->belongsTo(Municipio::class, 'municipio_id', 'codigo');
    }

    public function departamento()
    {
        return $this->belongsTo(Departamento::class, 'departamento_codigo', 'codigo');
    }


    public function scopeBuscarCoincidencia($query, $departamento, $municipios, $hecho_victimizante,
                                            $fecha_del_hecho, $victima, $denunciante, $id)
    {
        return $query->whereLike($this->filasBuscar, $departamento, $municipios, $hecho_victimizante
            , $fecha_del_hecho, $victima, $denunciante, $id);
    }

    public function fecha_del_hecho()
    {
        return $this->hasMany(FechaCaso::class);
    }

    //La misma fecha_del_hecho pero con otro nombre
    public function fechas()
    {
        return $this->hasMany(FechaCaso::class,'caso_id','id');
    }

    public function fecha_del_hecho_one()
    {
        return $this->hasOne(FechaCaso::class);
    }

    public function documento()
    {
        return $this->hasMany(DocumentoCasos::class);
    }

    public function responsable()
    {
        return $this->belongsTo(ResponsableHecho::class,'responsable_del_hecho_id','id');
    }

}

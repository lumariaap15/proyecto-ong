<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class FechaCaso extends Model implements AuditableContract
{
    use  Auditable;
    protected $fillable = [
        'tipo_fecha',
        'fecha_inicio',
        'fecha_final',
        'caso_id'
    ];


    public function caso(){
        return $this->belongsTo(Caso::class,'caso_id','id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class TipoDocumento extends Model implements AuditableContract
{

    use SoftDeletes, Auditable;

    protected $fillable = [
        'descripcion',
        'sigla'
    ];

}

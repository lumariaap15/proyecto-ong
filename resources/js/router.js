import VueRouter from 'vue-router'
import Layout from "./pages/layout/Layout";
import IndexCasos from "./pages/casos/index/IndexCasos";
import GeneralesIndex from "./pages/generales/GeneralesIndex";
import UsuariosIndex from "./pages/usuarios/UsuariosIndex";
import PersonasIndex from "./pages/personas/PersonasIndex";
import Reportes from "./pages/reportes/Reportes";
import Auditoria from "./pages/auditoria/Auditoria";
import Login from "./pages/layout/Login";
import Perfil from "./pages/perfil/Perfil";
import CasosWizard from "./pages/casos/crear/CasosWizard";
import EditarCaso from "./pages/casos/editar/EditarCaso";
import error404 from "./pages/errors/error404";
import error403 from "./pages/errors/error403";
import AuditoriaShow from "./pages/auditoria/AuditoriaShow";
import Backup from "./pages/backup/Backup";

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/login',
            name:'login',
            component: Login,
            meta:{roles:['administrador','casos','consulta']}
        },
        {path: '/sinpermisos', component: error403, name: 'sinpermisos',
        meta:{roles:['administrador','casos','consulta']}},
        {path: '*', component: error404, meta:{roles:['administrador','casos','consulta']}},
        {
            path:'/',
            component: Layout,
            redirect:'/casos',
            children:[
                {
                    path:'casos',
                    component: IndexCasos,
                    name: 'casos.index',
                    meta:{roles:['administrador','casos','consulta']}
                },
                {
                    path:'casos/crear',
                    component: CasosWizard,
                    name:'casos.create',
                    meta:{roles:['administrador','casos']}
                },
                {
                    path:'casos/:id',
                    component: EditarCaso,
                    name:'casos.editar',
                    meta:{roles:['administrador','casos','consulta']}
                },
                {
                    path:'general',
                    component: GeneralesIndex,
                    name:'generales.index',
                    meta:{roles:['administrador']}
                },
                {
                    path:'usuarios',
                    component: UsuariosIndex,
                    name:'usuarios.index',
                    meta:{roles:['administrador']}
                },
                {
                    path:'personas',
                    component: PersonasIndex,
                    name:'personas.index',
                    meta:{roles:['administrador']}
                },
                {
                    path:'reportes',
                    component: Reportes,
                    name:'reportes',
                    meta:{roles:['administrador','casos']}
                },
                {
                    path:'auditoria',
                    component: Auditoria,
                    name:'auditoria',
                    meta:{roles:['administrador','casos']}
                },
                {
                    path:'auditoria/:id',
                    component: AuditoriaShow,
                    name:'auditoria.show',
                    meta:{roles:['administrador','casos']}
                },
                {
                    path:'perfil',
                    component: Perfil,
                    name:'perfil',
                    meta:{roles:['administrador','casos','consulta']}
                },
                {
                    path:'backups',
                    component: Backup,
                    name:'backups',
                    meta:{roles:['administrador']}
                },
            ],
        },
    ]
});

router.beforeEach((to, from, next) => {
    const user = JSON.parse(localStorage.getItem('user'));
    if(to.path !== "/login") {
        if (user === null) {
            next({name: 'login'});
        } else {
            if(to.meta.roles.includes(user.role)) {
                next();
            }else{
                next({name: 'sinpermisos'})
            }
        }
    }else{
        if(user !== null){
            if(to.meta.roles.includes(user.role)) {
                next({name: 'casos.index'});
            }else{
                next({name: 'sinpermisos'})
            }
        }else{
            next()
        }
    }
});


export default router;

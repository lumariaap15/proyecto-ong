import Vue from 'vue';
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state:{
        tiposDeDocumentos:[],
        gruposEtnicos:[],
        gruposIndigenas:[],
        Organizaciones:[],
        estadosCiviles:[],
        gradosDeEscolaridad:[],
        hechosVictimizantes:[],
        comunidades:[],
        responsables:[],
        estados:[],
        tablasDescargadas:[]
    },
    mutations:{
        //TIPOS DE DOCUMENTOS
        agregarTipoDocumento(state, tipoDocumento){
            state.tiposDeDocumentos = [...state.tiposDeDocumentos, tipoDocumento]
        },
        actualizarTipoDocumento(state, tipoDocumento){
            let tipos = state.tiposDeDocumentos;
            tipos = tipos.map(item => {
                if(item.id === tipoDocumento.id){
                    return tipoDocumento.data
                }else{
                    return item
                }
            });
            state.tiposDeDocumentos = tipos
        },
        eliminarTipoDocumento(state, id){
            let tipos = state.tiposDeDocumentos;
            tipos = tipos.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.tiposDeDocumentos = tipos
        },
        //ESTADOS CIVILES
        agregarEstadoCivil(state, item){
            state.estadosCiviles = [...state.estadosCiviles, item]
        },
        actualizarEstadoCivil(state, estadoCivil){
            let items = state.estadosCiviles;
            items = items.map(item => {
                if(item.id === estadoCivil.id){
                    return estadoCivil.data
                }else{
                    return item
                }
            });
            state.estadosCiviles = items
        },
        eliminarEstadoCivil(state, id){
            let items = state.estadosCiviles;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.estadosCiviles = items
        },
        //GRADOS DE ESCOLARIDAD
        agregarGradoEscolaridad(state, item){
            state.gradosDeEscolaridad = [...state.gradosDeEscolaridad, item]
        },
        actualizarGradoEscolaridad(state, gradoEscolaridad){
            let items = state.gradosDeEscolaridad;
            items = items.map(item => {
                if(item.id === gradoEscolaridad.id){
                    return gradoEscolaridad.data
                }else{
                    return item
                }
            });
            state.gradosDeEscolaridad = items
        },
        eliminarGradoEscolaridad(state, id){
            let items = state.gradosDeEscolaridad;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.gradosDeEscolaridad = items
        },
        //GRUPOS ÉTNICOS
        agregarGrupoEtnico(state, item){
            state.gruposEtnicos = [...state.gruposEtnicos, item]
        },
        actualizarGrupoEtnico(state, grupo){
            let items = state.gruposEtnicos;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.gruposEtnicos = items
        },
        eliminarGrupoEtnico(state, id){
            let items = state.gruposEtnicos;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.gruposEtnicos = items
        },
        //GRUPOS INDÍGENAS
        agregarGrupoIndigena(state, item){
            state.gruposIndigenas = [...state.gruposIndigenas, item]
        },
        actualizarGrupoIndigena(state, grupo){
            let items = state.gruposIndigenas;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.gruposIndigenas = items
        },
        eliminarGrupoIndigena(state, id){
            let items = state.gruposIndigenas;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.gruposIndigenas = items
        },
        //ORGANIZACIONES
        agregarOrganizacion(state, item){
            state.Organizaciones = [...state.Organizaciones, item]
        },
        actualizarOrganizacion(state, grupo){
            let items = state.Organizaciones;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.Organizaciones = items
        },
        eliminarOrganizacion(state, id){
            let items = state.Organizaciones;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.Organizaciones = items
        },
        //HECHOS VICTIMIZANTES
        agregarHechoVictimizante(state, item){
            state.hechosVictimizantes = [...state.hechosVictimizantes, item]
        },
        actualizarHechoVictimizante(state, grupo){
            let items = state.hechosVictimizantes;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.hechosVictimizantes = items
        },
        eliminarHechoVictimizante(state, id){
            let items = state.hechosVictimizantes;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.hechosVictimizantes = items
        },
        //COMUNIDADES
        agregarComunidad(state, item){
            state.comunidades = [...state.comunidades, item]
        },
        actualizarComunidad(state, grupo){

            let items = state.comunidades;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.comunidades = items
        },
        eliminarComunidad(state, id){
            let items = state.comunidades;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.comunidades = items
        },
        //RESPONSABLES
        agregarResponsable(state, item){
            state.responsables = [...state.responsables, item]
        },
        actualizarResponsable(state, grupo){

            let items = state.responsables;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.responsables = items
        },
        eliminarResponsable(state, id){
            let items = state.responsables;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.responsables = items
        },
        //ESTADOS
        agregarEstado(state, item){
            state.estados = [...state.estados, item]
        },
        actualizarEstado(state, grupo){
            let items = state.estados;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.estados = items
        },
        eliminarEstado(state, id){
            let items = state.estados;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.estados = items
        },
    },
    getters:{
        tiposDeDocumentos: state => state.tiposDeDocumentos,
        gruposEtnicos: state => state.gruposEtnicos,
        gruposIndigenas: state => state.gruposIndigenas,
        Organizaciones: state => state.Organizaciones,
        estadosCiviles: state => state.estadosCiviles,
        gradosDeEscolaridad: state => state.gradosDeEscolaridad,
        hechosVictimizantes: state => state.hechosVictimizantes,
        comunidades: state => state.comunidades,
        responsables: state => state.responsables,
        estados: state => state.estados,
    }
});

import axios from './GlobalService';

const CasoService = {
    all(query) {
        return axios.get('caso'+query);
    },
    store(data) {
        return axios.post('caso', data, {
            headers: {
                'Content-Type': 'application/json',
            }
        });
    },
    find(id) {
        return axios.get(`caso/${id}`);
    },
    update(id, data) {
        return axios.put(`caso/${id}`, data);
    },
    delete(id) {
        return axios.delete(`caso/${id}`);
    },
};

export default CasoService;

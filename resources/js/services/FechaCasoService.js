import axios from './GlobalService';

const FechaCasoService = {
    store(casoId,data) {
        return axios.post('fecha_caso/caso/'+casoId, data);
    },
    delete(id) {
        return axios.delete(`fecha_caso/fecha/${id}`);
    },
};

export default FechaCasoService;

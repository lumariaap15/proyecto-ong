import axios from './GlobalService';

const VersionCasoService = {
    all(query) {
        return axios.get('versioncasos'+query);
    },
    allAuth(query) {
        return axios.get('versioncasos/user/mis_versiones'+query);
    },
    find(id) {
        return axios.get(`versioncasos/${id}`);
    },
    update(id, estado) {
        return axios.put(`versioncasos/${id}`, {estado: estado});
    },
};

export default VersionCasoService;

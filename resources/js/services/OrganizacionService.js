import axios from './GlobalService';

const OrganizacionService = {
    all() {
        return axios.get('organizaciones');
    },
    store(data) {
        return axios.post('organizaciones', data);
    },
    find(id) {
        return axios.get(`organizaciones/${id}`);
    },
    update(id, data) {
        return axios.put(`organizaciones/${id}`, data);
    },
    delete(id) {
        return axios.delete(`organizaciones/${id}`);
    },
};

export default OrganizacionService;

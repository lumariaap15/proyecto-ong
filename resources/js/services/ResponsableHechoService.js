import axios from './GlobalService';
import {store} from "../store/store";

const ResponsableService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.responsables.length === 0) {
                let response = axios.get('responsableHecho');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarResponsable', item);
                    });
                    resolve({data: store.getters.responsables})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.responsables})
            }
        })
    },
    store(data) {
        return axios.post('responsableHecho', data);
    },
    find(id) {
        return axios.get(`responsableHecho/${id}`);
    },
    update(id, data) {
        //store.commit('actualizarResponsable',{id: id, data: data});
        return axios.put(`responsableHecho/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarResponsable',id);
        return axios.delete(`responsableHecho/${id}`);
    },
};

export default ResponsableService;

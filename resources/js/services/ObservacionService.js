import axios from './GlobalService';

const ObservacionService = {
    all(idCaso,query) {
        return axios.get(`observacionCaso/caso/${idCaso+query}`);
    },
    store(data) {
        return axios.post('observacionCaso', data);
    },
    update(id, data) {
        return axios.put(`observacionCaso/${id}`, data);
    },
    delete(id, data) {
        return axios.delete(`observacionCaso/${id}`, data);
    },
};

export default ObservacionService;

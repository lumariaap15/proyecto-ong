import axios from './GlobalService';
import {store} from "../store/store";

const ComunidadService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.comunidades.length === 0) {
                let response = axios.get('comunidad');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarComunidad', item);
                    });
                    resolve({data: store.getters.comunidades})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.comunidades})
            }
        })
    },
    store(data) {
        return axios.post('comunidad', data);
    },
    find(id) {
        return axios.get(`comunidad/${id}`);
    },
    update(id, data) {
        return axios.put(`comunidad/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarComunidad',id);
        return axios.delete(`comunidad/${id}`);
    },
};

export default ComunidadService;

import axios from './GlobalService';

const HechoVictimizanteService = {
    all() {
        return axios.get('hechos_victimizantes');
    },
    store(data) {
        return axios.post('hechos_victimizantes', data);
    },
    find(id) {
        return axios.get(`hechos_victimizantes/${id}`);
    },
    update(id, data) {
        return axios.put(`hechos_victimizantes/${id}`, data);
    },
    delete(id) {
        return axios.delete(`hechos_victimizantes/${id}`);
    },
};

export default HechoVictimizanteService;

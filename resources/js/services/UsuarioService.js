import axios from './GlobalService';

const UsuarioService = {
    all(query) {
        return axios.get('usuarios'+query);
    },
    store(data) {
        return axios.post('usuarios', data);
    },
    find(id) {
        return axios.get(`usuarios/${id}`);
    },
    update(id, data) {
        return axios.put(`usuarios/${id}`, data);
    },
    delete(id) {
        return axios.delete(`usuarios/${id}`);
    },
    changePassword(data) {
        return axios.put('usuarios/password', data);
    },
};

export default UsuarioService;

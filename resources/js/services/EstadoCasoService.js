import axios from './GlobalService';
import {store} from "../store/store";

const EstadoCasoService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.estados.length === 0) {
                let response = axios.get('estado_procesos');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarEstado', item);
                    });
                    resolve({data: store.getters.estados})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.estados})
            }

        })
    },
    store(data) {
        return axios.post('estado_procesos', data);
    },
    find(id) {
        return axios.get(`estado_procesos/${id}`);
    },
    update(id, data) {
        return axios.put(`estado_procesos/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarEstado',id);
        return axios.delete(`estado_procesos/${id}`);
    },
};

export default EstadoCasoService;

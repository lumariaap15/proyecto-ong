import axios from './GlobalService';

const NotificacionService = {
    all(query) {
        return axios.get('notificaciones'+query);
    },
    update(id, estado) {
        return axios.put(`notificaciones/${id}`, {visto: estado});
    },
};

export default NotificacionService;

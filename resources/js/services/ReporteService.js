import axios from './GlobalService';

const ReporteService = {
    municipios(query) {
        return axios.get('reporte/municipios'+query);
    },
    hechosVictimizantes(query) {
        return axios.get('reporte/hechosVictimizantes'+query);
    },
    hechosVictimizantesComparativo(query) {
        return axios.get('reporte/hechosVictimizantesComparativo'+query);
    },
    genero(query) {
        return axios.get('reporte/genero'+query);
    },
    responsablesHecho(query) {
        return axios.get('reporte/responsablesHecho'+query);
    },
    responsablesHechoComparativo(query) {
        return axios.get('reporte/hechosResponsablesHechoComparativo'+query);
    },
    liderazgo(query) {
        return axios.get('reporte/liderazgo'+query);
    },
    general(query) {
        /*
        axios.responseType = 'arraybuffer';
        axios.headers = {'Accept': 'application/vnd.ms-excel'}
        return axios.get('reporte/general'+query);

         */
        window.open('/api/reporte/general'+query,'_blank')
    },
};

export default ReporteService;

import axios from './GlobalService';
import {store} from "../store/store";

const GrupoIndigenaService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.gruposIndigenas.length === 0) {
                let response = axios.get('grupo_indigenas');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarGrupoIndigena', item);
                    });
                    resolve({data: store.getters.gruposIndigenas})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.gruposIndigenas})
            }
        })
    },
    store(data) {
        return axios.post('grupo_indigenas', data);
    },
    find(id) {
        return axios.get(`grupo_indigenas/${id}`);
    },
    update(id, data) {
        //store.commit('actualizarGrupoIndigena',{id: id, data: data});
        return axios.put(`grupo_indigenas/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarGrupoIndigena',id);
        return axios.delete(`grupo_indigenas/${id}`);
    },
};

export default GrupoIndigenaService;

import axios from './GlobalService';
import {store} from "../store/store";

const GradoEscolaridadService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.gradosDeEscolaridad.length === 0) {
                let response = axios.get('grado_escolaridades');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarGradoEscolaridad', item);
                    });
                    resolve({data: store.getters.gradosDeEscolaridad})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.gradosDeEscolaridad})
            }
        })
    },
    store(data) {
        return axios.post('grado_escolaridades', data);
    },
    find(id) {
        return axios.get(`grado_escolaridades/${id}`);
    },
    update(id, data) {
        //store.commit('actualizarGradoEscolaridad',{id: id, data: data});
        return axios.put(`grado_escolaridades/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarGradoEscolaridad',id);
        return axios.delete(`grado_escolaridades/${id}`);
    },
};

export default GradoEscolaridadService;

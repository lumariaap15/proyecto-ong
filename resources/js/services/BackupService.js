import axios from './GlobalService';

const BackupService = {
    all() {
        return axios.get('backup');
    },
    store() {
        return axios.post('backup');
    },
    download(name) {
        /*
        return axios.get(`backup/download/${name}`);

         */
        window.open(`/api/backup/download/${name}`,'_blank')
    },
    delete(name) {
        return axios.delete(`backup/${name}`);
    },
};

export default BackupService;

import axios from './GlobalService';

const DocumentoCasoService = {
    all(tipo,id,tipo_documento, nombre) {
        return axios.get(`documentoCaso/${tipo}/${id}?tipo=${tipo_documento}&nombre=${nombre}`);
    },
    store(data) {
        return axios.post('documentoCaso', data, {
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        });
    },
    update(id, data) {
        return axios.post(`documentoCaso/${id}`, data, {
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        });
    },
    delete(id, data) {
        return axios.delete(`documentoCaso/${id}`, data);
    },
};

export default DocumentoCasoService;

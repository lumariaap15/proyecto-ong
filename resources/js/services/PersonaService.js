import axios from './GlobalService';

const PersonaService = {
    all(query) {
        return axios.get('personas' + query);
    },
    store(data) {
        return axios.post('personas', data, {
            headers: {
                'Content-Type': 'application/json',
            }
        });
    },
    find(id) {
        return axios.get(`personas/${id}`);
    },
    update(id, data) {
        return axios.put(`personas/${id}`, data);
    },
    delete(id) {
        return axios.delete(`personas/${id}`);
    },
};

export default PersonaService;

require('./bootstrap');
require('popper.js/dist/umd/popper.min');
require('jquery/dist/jquery.min')

window.Vue = require('vue');
import VueRouter from 'vue-router'
import VueSwal from 'vue-swal'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)

// Import component
import Loading from 'vue-loading-overlay';
// Import stylesheet
import 'vue-loading-overlay/dist/vue-loading.css';
// Init plugin
Vue.component('Loading',Loading)

import { ValidationProvider, extend, ValidationObserver } from 'vee-validate';

// Register it globally
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

import * as rules from 'vee-validate/dist/rules';
import es from 'vee-validate/dist/locale/es';

// loop over all rules
for (let rule in rules) {
    extend(rule, {
        ...rules[rule], // add the rule
        message: es.messages[rule] // add its message
    });
}

//FILTROS (PIPES)
Vue.filter('capitalize', function (value) {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
});

import vSelect from 'vue-select'

Vue.component('v-select', vSelect)

Vue.use(VueSwal)

Vue.component('principal-component', require('./components/PrincipalComponent.vue').default);
Vue.use(VueRouter);

Vue.mixin({
    methods:{
        hasRole(name){
            return JSON.parse(localStorage.getItem('user')).role === name
        }
    }
});

import router from './router.js';
import { store } from './store/store';

require('moment')


const app = new Vue({
    el: '#app',
    router,
    store
});

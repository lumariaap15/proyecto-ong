<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::put('usuarios/password','UserController@changePassword');
Route::apiResource('usuarios','UserController');
Route::apiResource('tipo_documentos','TipoDocumentoController');
Route::apiResource('estado_civiles','EstadoCivilController');
Route::apiResource('grado_escolaridades','GradoEscolaridadController');
Route::apiResource('grupo_etnicos','GrupoEtnicoController');
Route::apiResource('grupo_indigenas','GrupoIndigenaController');

Route::get('personas/{id}/casos','PersonaController@casos');
Route::apiResource('personas','PersonaController');
Route::apiResource('organizaciones', 'OrganizacionController');
Route::apiResource('hechos_victimizantes', 'HechoVictimizanteController');
Route::apiResource('caso','CasoController');
Route::apiResource('comunidad','ComunidadController');
Route::apiResource('responsableHecho','ResponsableHechoController');
Route::get('reporte/municipios','ReportesController@getReportePorMunicipios');
Route::get('reporte/hechosVictimizantes','ReportesController@getReporteHechosVictimizante');
Route::get('reporte/hechosVictimizantesComparativo','ReportesController@getReporteHechosVictimizanteComparativo');
Route::get('reporte/hechosResponsablesHechoComparativo','ReportesController@getReporteResponsablesHechoComparativo');
Route::get('reporte/genero','ReportesController@getReportePorGenero');
Route::get('reporte/responsablesHecho','ReportesController@getReporteResponsablesHecho');
Route::get('reporte/liderazgo','ReportesController@getReportePorLiderazgo');
Route::get('reporte/general','ReportesController@getReporteGeneral');

Route::apiResource('observacionCaso','ObservacionCasoController')->except(['index']);
Route::get('observacionCaso/caso/{idcaso}','ObservacionCasoController@index');

//Route::apiResource('documentoCaso','DocumentoCasoController');
Route::post('documentoCaso','DocumentoCasoController@store');
Route::get('documentoCaso/{tipo}/{id}','DocumentoCasoController@index');
Route::put('documentoCaso/{id}','DocumentoCasoController@update');
Route::delete('documentoCaso/{id}','DocumentoCasoController@destroy');

Route::get('departamentos','DepartamentoController@index');
Route::get('municipios/{codigo}','MunicipioController@index');

Route::post('fecha_caso/caso/{caso_id}','FechaCasoController@store');
Route::put('fecha_caso/fecha/{fecha_id}','FechaCasoController@update');
Route::delete('fecha_caso/fecha/{fecha_id}','FechaCasoController@destroy');

Route::get('versioncasos','VersionCasoController@index');
Route::get('versioncasos/user/mis_versiones','VersionCasoController@misVersiones');
Route::get('versioncasos/{id}','VersionCasoController@show');
Route::put('versioncasos/{id}','VersionCasoController@confirmarVersion');


Route::get('notificaciones','NotificacionController@index');
Route::put('notificaciones/{id}','NotificacionController@update');
Route::put('notificaciones/allvisto','NotificacionController@updateAllvisto');

Route::apiResource('estado_procesos','EstadoProcesoCasoController');

Route::get('backup','BackupController@index');
Route::post('backup','BackupController@store');
Route::delete('backup/{file_name}','BackupController@delete');
Route::get('backup/download/{file_name}', 'BackupController@download');

//Route::get('personas','PersonaController@index');


Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout');
Route::post('refresh', 'AuthController@refresh');
